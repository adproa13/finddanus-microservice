[![pipeline status](https://gitlab.com/adproa13/finddanus-microservice/badges/master/pipeline.svg)](https://gitlab.com/adproa13/finddanus-microservice/-/commits/master)
[![coverage report](https://gitlab.com/adproa13/finddanus-microservice/badges/master/coverage.svg)](https://gitlab.com/adproa13/finddanus-microservice/-/commits/master)

# Find Danus
We’re here to facilitate organizations or committees to increase their income from selling Foods or Drinks.

This platform also facilitates sellers who don’t know where to sell their products, especially foods and drinks. So, they can also increase their income. 

## Features
* Administration (Login & Register)
```concept
A feature for user to create account and login
to use this app.
```
Implemented by:
*Steven Ciayadi - 1806205104*

* Seeker
```concept
A feature for user to search foods or drinks (Danus)
for their organization to increase their income by 
reselling products.
```
Implemented by:
*Muhammad Rifqi - 1806205621*

* Seller
```concept
A feature for seller to provide products to the seeker.
```
Implemented by:
*Michael Susanto - 1806205653*

* Chat
```concept
A feature for seeker and seller to communicate each other.
```
Implemented by:
*Giffari Faqih Phrasya Hardani - 1806205634*

## Description
FindDanus will be consisted of 4 different services:
* Gateway service
    * API gateway service routes all requests to other services & do authentication-authorization.
* User service
    * User service handles all seeker's & seller's functionalities.
* Chat service
    * Chat service handles chat's functionalities.
* Service registry
    * Acts as Eureka Server to save the address of all connected services.
    
The project itself have 5 modules:
* finddanus-domain
    * A module for all business logic used by services.
* [finddanus-service-chat](https://finddanus-service-chat.herokuapp.com)
    * A service for chat feature.
* [finddanus-service-gateway](https://finddanus-service-gateway.herokuapp.com)
    * A service that routes all requests to correct service.
* [finddanus-service-user](https://finddanus-service-user.herokuapp.com)
    * A service that handles user's features (seeker & seller).
* [finddanus-service-registry](https://finddanus-service-registry.herokuapp.com)
    * A service that saves all the address of other services.

# How to run
* Clone this repository
```cmd
git clone https://gitlab.com/adproa13/finddanus-microservice.git
```

* Open this project in your IDE (Intellij IDEA is recommended).

* You can tick enable auto-import if you want, but you can do it manually by click "Import changes" in the bottom-right after you opened your project.

* Use default gradle wrapper and click open.

* Wait Intellij IDEA to build the project.

* Go to finddanus-service-registry/src/main/java/com/finddanus/server/ServiceRegistryApplication.java and click run.

* Go to finddanus-service-gateway/src/main/java/com/finddanus/chatservice/GatewayServiceApplication.java and click run.

* Go to finddanus-service-chat/src/main/java/com/finddanus/chatservice/ChatServiceApplication.java and click run.

* Go to finddanus-service-user/src/main/java/com/finddanus/chatservice/UserServiceApplication.java and click run.

* Open:
    * localhost:8761 for service registry.
    * localhost:8081 for chat service.
    * localhost:8082 for gateway service.
    * localhost:8083 for user service.
    
Best regards,
Find Danus Team.