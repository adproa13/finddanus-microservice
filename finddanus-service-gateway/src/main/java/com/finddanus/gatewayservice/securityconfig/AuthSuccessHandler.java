package com.finddanus.gatewayservice.securityconfig;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AuthSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
  @Override
  protected String determineTargetUrl(HttpServletRequest request,
                                      HttpServletResponse response,
                                      Authentication authentication) {
    String role = authentication.getAuthorities().toString();
    String targetUrl = "";
    if (role.contains("SEEKER")) {
      targetUrl = "/home";
    } else if (role.contains("SELLER")) {
      targetUrl = "/seller/home";
    }
    return targetUrl;
  }
}
