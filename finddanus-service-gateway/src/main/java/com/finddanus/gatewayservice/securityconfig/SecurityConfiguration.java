package com.finddanus.gatewayservice.securityconfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.header.writers.StaticHeadersWriter;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  @Autowired
  private UserDetailsService userDetailsService;

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService);
  }

  @Override
  public void configure(WebSecurity web) throws Exception {
    web.ignoring().antMatchers("/js/**", "/css/**", "/fonts/**", "/img/**");
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf().disable()
        .authorizeRequests()
        .antMatchers("/register").permitAll()
        .antMatchers("/").permitAll()
        .antMatchers("/seller/preview/**").hasRole("SEEKER")
        .antMatchers("/seller/**").hasRole("SELLER")
        .antMatchers("/preview/**").hasRole("SELLER")
        .antMatchers("/**").hasRole("SEEKER")
        .and().formLogin().successHandler(authSuccessHandler())
        .and().logout().logoutSuccessUrl("/").deleteCookies("JSESSIONID");
    http.headers().addHeaderWriter(
        new StaticHeadersWriter("Access-Control-Allow-Origin", "*"));
  }

  @Bean
  public PasswordEncoder getPasswordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean
  public AuthSuccessHandler authSuccessHandler() {
    return new AuthSuccessHandler();
  }
}
