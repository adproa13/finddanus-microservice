package com.finddanus.gatewayservice.controller;

import com.finddanus.domain.chat.model.Chat;
import com.finddanus.domain.chat.service.ChatServiceImpl;
import com.finddanus.domain.response.ChatResponse;
import com.finddanus.domain.seeker.service.SeekerService;
import com.finddanus.domain.request.ChatRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ChatController {

  @Autowired
  private RestTemplate restTemplate;

  public ChatController(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  @GetMapping("/chat/{chatId}")
  public String getSeekerChat(
      @PathVariable long chatId,
      Model model
  ) {
    Chat response = restTemplate.getForObject("http://finddanus-service-chat/chat/" + chatId, Chat.class);
    model.addAttribute("chat", response);
    return "chatSeeker";
  }

  @GetMapping("/seller/chat/{chatId}")
  public String getSellerChat(
      @PathVariable long chatId,
      Model model
  ) {
    Chat response = restTemplate.getForObject("http://finddanus-service-chat/chat/" + chatId, Chat.class);
    model.addAttribute("chat", response);
    return "chatSeller";
  }

  /**
   * Post mapping when seeker sends message.
   * @param redirectAttributes of attributes
   * @param seekerId of seeker
   * @param chatId of chat
   * @param message of seeker
   * @return redirects to chat view
   */
  @PostMapping("/chat/send")
  public String seekerSendMessage(
      RedirectAttributes redirectAttributes,
      @RequestParam(value = "seeker_id") long seekerId,
      @RequestParam(value = "chat_id") long chatId,
      @RequestParam(value = "message") String message
  ) {
    if (seekerId != -1) {
      Chat response = restTemplate.postForObject(
          "http://finddanus-service-chat/chat/send",
          new ChatRequest(chatId, seekerId, message),
          Chat.class
      );
      redirectAttributes.addAttribute("chat", response);
      redirectAttributes.addAttribute("chatId", chatId);
      return "redirect:/chat/{chatId}";
    }
    return "redirect:/chats";
  }

  /**
   * Post mapping when seller sends message.
   * @param redirectAttributes of attributes
   * @param sellerId of seller
   * @param chatId of chat
   * @param message of seller
   * @return redirect to chat view again
   */
  @PostMapping("seller/chat/send")
  public String sellerSendMessage(
      RedirectAttributes redirectAttributes,
      @RequestParam(value = "seller_id") long sellerId,
      @RequestParam(value = "chat_id") long chatId,
      @RequestParam(value = "message") String message
  ) {
    if (sellerId != -1) {
      Chat response = restTemplate.postForObject(
          "http://finddanus-service-chat/seller/chat/send",
          new ChatRequest(chatId, sellerId, message),
          Chat.class
      );
      redirectAttributes.addAttribute("chat", response);
      redirectAttributes.addAttribute("chatId", chatId);
      return "redirect:/seller/chat/{chatId}";
    }
    return "redirect:/seller/chats";
  }
}
