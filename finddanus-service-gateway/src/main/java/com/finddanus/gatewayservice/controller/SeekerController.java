package com.finddanus.gatewayservice.controller;

import com.finddanus.domain.chat.model.Chat;
import com.finddanus.domain.request.EditPasswordRequest;
import com.finddanus.domain.request.EditProfileRequest;
import com.finddanus.domain.request.FollowUnfollowRequest;
import com.finddanus.domain.seeker.model.Seeker;
import com.finddanus.domain.seeker.service.SeekerService;
import com.finddanus.domain.seller.model.Product;
import com.finddanus.domain.seller.model.Subject;
import com.finddanus.gatewayservice.userdetails.FindDanusUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Controller
public class SeekerController {

  @Autowired
  private RestTemplate restTemplate;

  @Autowired
  private final PasswordEncoder passwordEncoder;

  public SeekerController(RestTemplate restTemplate, PasswordEncoder passwordEncoder) {
    this.restTemplate = restTemplate;
    this.passwordEncoder = passwordEncoder;
  }

  /**
   * Returns seeker's homepage.
   * @param model for the view
   * @return seeker/seekerHomepage.html
   */
  @GetMapping(path = "/home")
  public String seekerHomepage(Model model, HttpServletRequest request, Authentication auth) {
    long id = ((FindDanusUserDetails) auth.getPrincipal()).getId();
    request.getSession().setAttribute("id", id);
    ResponseEntity<List<Product>> responseProductList = restTemplate.exchange(
        "http://finddanus-service-user/" + id + "/home",
        HttpMethod.GET,
        null,
        new ParameterizedTypeReference<List<Product>>() {}
    );
    List<Product> products = responseProductList.getBody();
    try {
      Seeker seekerResponse = restTemplate.getForObject(
          "http://finddanus-service-user/" + id,
          Seeker.class
      );
      model.addAttribute("seekerId", id);
      model.addAttribute("seekerLocation", seekerResponse.getLocation());
      model.addAttribute("products", products);
    } catch (NullPointerException e) {
      model.addAttribute("notFound", true);
    }
    return "seeker/seekerHomepage";
  }

  /**
   * Returns seeker's profile page.
   * @param model for the view
   * @return seeker/seekerProfile.html
   */
  @GetMapping("/profile")
  public String seekerProfile(Model model, HttpServletRequest request) {
    long id = (long) request.getSession().getAttribute("id");
    try {
      Seeker seekerResponse = restTemplate.getForObject(
          "http://finddanus-service-user/" + id,
          Seeker.class
      );
      model.addAttribute("seeker", seekerResponse);
    } catch (NullPointerException e) {
      model.addAttribute("notFound", true);
    }
    return "seeker/seekerProfile";
  }

  @PostMapping("/home")
  public String searchProduct(
      RedirectAttributes redirectAttributes,
      @RequestParam(value = "productName") String productName,
      HttpServletRequest request
  ) {
    redirectAttributes.addAttribute("productName", productName);
    return "redirect:/search/productName={productName}";
  }

  /**
   * Returns products with specific name.
   * @param productName for product's name
   * @param model for the view
   * @return seeker/seekerSearchResult.html
   */
  @GetMapping("/search/productName={productName}")
  public String showProductByName(
      @PathVariable String productName,
      Model model,
      HttpServletRequest request
  ) {
    long id = (long) request.getSession().getAttribute("id");
    ResponseEntity<List<Product>> responseProductListByName = restTemplate.exchange(
        "http://finddanus-service-user/" + id + "/product=" + productName,
        HttpMethod.GET,
        null,
        new ParameterizedTypeReference<List<Product>>() {}
    );
    List<Product> products = responseProductListByName.getBody();

    ResponseEntity<List<Subject>> responseFollowing = restTemplate.exchange(
        "http://finddanus-service-user/" + id + "/following",
        HttpMethod.GET,
        null,
        new ParameterizedTypeReference<List<Subject>>() {}
    );
    List<Subject> seekerFollowing = responseFollowing.getBody();
    List<Long> seekerFollowingId = new ArrayList<>();
    for (Subject s : seekerFollowing) {
      seekerFollowingId.add(s.getId());
    }

    model.addAttribute("listFollowing", seekerFollowingId);
    model.addAttribute("products", products);
    model.addAttribute("keyword", productName);
    model.addAttribute("seekerId", id);
    return "seeker/seekerSearchResult";
  }

  /**
   * Returns products with specific location.
   * @param seekerLocation for product's name
   * @param model for the view
   * @return seeker/seekerSearchResult.html
   */
  @GetMapping("/search/location={seekerLocation}")
  public String showProductByLocation(
      @PathVariable String seekerLocation,
      Model model,
      HttpServletRequest request
  ) {
    long id = (long) request.getSession().getAttribute("id");
    ResponseEntity<List<Product>> responseProductListByLocation = restTemplate.exchange(
        "http://finddanus-service-user/" + id + "/location=" + seekerLocation,
        HttpMethod.GET,
        null,
        new ParameterizedTypeReference<List<Product>>() {}
    );
    List<Product> products = responseProductListByLocation.getBody();
    ResponseEntity<List<Subject>> responseFollowing = restTemplate.exchange(
        "http://finddanus-service-user/" + id + "/following",
        HttpMethod.GET,
        null,
        new ParameterizedTypeReference<List<Subject>>() {}
    );
    List<Subject> seekerFollowing = responseFollowing.getBody();
    List<Long> seekerFollowingId = new ArrayList<>();
    for (Subject s : seekerFollowing) {
      seekerFollowingId.add(s.getId());
    }

    model.addAttribute("listFollowing", seekerFollowingId);
    model.addAttribute("products", products);
    model.addAttribute("keyword", seekerLocation);
    model.addAttribute("seekerId", id);
    return "seeker/seekerSearchResult";
  }

  /**
   * Follow Seller then return to previous url.
   * @param sellerId the target seller's id
   * @return redirect to previous url (search results page)
   */
  @GetMapping("/follow/{sellerId}")
  public String seekerFollowSeller(
      @PathVariable long sellerId,
      HttpServletRequest request
  ) {
    long id = (long) request.getSession().getAttribute("id");
    ResponseEntity responseFollow = restTemplate.postForObject(
        "http://finddanus-service-user/" + sellerId + "/follow",
        new FollowUnfollowRequest(id, sellerId),
        ResponseEntity.class
    );
    return "redirect:" + request.getHeader("Referer");
  }


  /**
   * Unfollow Seller then return to previous url.
   * @param sellerId the target seller's id
   * @return redirect to previous url (following page)
   */
  @GetMapping("/unfollow/{sellerId}")
  public String seekerUnfollowSeller(
      @PathVariable long sellerId,
      HttpServletRequest request
  ) {
    long id = (long) request.getSession().getAttribute("id");
    ResponseEntity responseUnfollow = restTemplate.postForObject(
        "http://finddanus-service-user/" + sellerId + "/unfollow",
        new FollowUnfollowRequest(id, sellerId),
        ResponseEntity.class
    );
    return "redirect:" + request.getHeader("Referer");
  }

  /**
   * Returns seeker's edit profile page.
   * @param model for the view
   * @return seeker/seekerEdit.html
   */
  @GetMapping("/edit")
  public String editProfilePage(Model model, HttpServletRequest request) {
    long id = (long) request.getSession().getAttribute("id");
    try {
      Seeker seekerResponse = restTemplate.getForObject(
          "http://finddanus-service-user/" + id,
          Seeker.class
      );
      model.addAttribute("seeker", seekerResponse);
      return "seeker/seekerEdit";
    } catch (NoSuchElementException e) {
      return "redirect:/profile";
    }
  }

  /**
   * Returns seeker's following page.
   * @param model for the view
   * @return seeker/seekerFollowing.html
   */
  @GetMapping("/following")
  public String seekerFollowingPage(Model model, HttpServletRequest request) {
    long id = (long) request.getSession().getAttribute("id");
    try {
      ResponseEntity<List<Subject>> responseFollowing = restTemplate.exchange(
          "http://finddanus-service-user/" + id + "/following",
          HttpMethod.GET,
          null,
          new ParameterizedTypeReference<List<Subject>>() {}
      );
      List<Subject> seekerFollowing = responseFollowing.getBody();
      Seeker seekerResponse = restTemplate.getForObject(
          "http://finddanus-service-user/" + id,
          Seeker.class
      );
      model.addAttribute("seeker", seekerResponse);
      model.addAttribute("seekerFollowing", seekerFollowing);
    } catch (NullPointerException e) {
      model.addAttribute("notFound", true);
    }
    return "seeker/seekerFollowing";
  }

  /**
   * Update profile to database.
   * @param seekerId of seeker
   * @param name of seeker
   * @param email of seeker
   * @param location of seeker
   * @param phoneNum of seeker
   * @param passwordConfirm of seeker
   * @return redirect to seeker profile
   */
  @PostMapping("/edit")
  public String updateProfile(
      @RequestParam(value = "seeker_id") long seekerId,
      @RequestParam(value = "name") String name,
      @RequestParam(value = "email") String email,
      @RequestParam(value = "location") String location,
      @RequestParam(value = "phoneNum") String phoneNum,
      @RequestParam(value = "passwordConfirm") String passwordConfirm
  ) {
    Seeker seekerResponse = restTemplate.getForObject(
        "http://finddanus-service-user/" + seekerId,
        Seeker.class
    );
    if (passwordEncoder.matches(passwordConfirm, seekerResponse.getPassword())) {
      restTemplate.put(
          "http://finddanus-service-user/" + seekerId,
          new EditProfileRequest(name, email, passwordConfirm, location, phoneNum)
      );
    }
    return "redirect:/profile";
  }

  /**
   * Update profile to database.
   * @param seekerId of seeker
   * @param oldPassword of seeker
   * @param newPassword of seeker
   * @param confirmNewPassword of seeker
   * @return redirect to seeker profile
   */
  @PostMapping("/editPass")
  public String updatePassword(
      @RequestParam(value = "seeker_id") long seekerId,
      @RequestParam(value = "oldPassword") String oldPassword,
      @RequestParam(value = "newPassword") String newPassword,
      @RequestParam(value = "confirmNewPassword") String confirmNewPassword
  ) {
    Seeker seekerResponse = restTemplate.getForObject(
        "http://finddanus-service-user/" + seekerId,
        Seeker.class
    );
    if (passwordEncoder.matches(oldPassword, seekerResponse.getPassword())
        && newPassword.equals(confirmNewPassword)) {
      restTemplate.put(
          "http://finddanus-service-user/" + seekerId + "/pass",
          new EditPasswordRequest(oldPassword, passwordEncoder.encode(newPassword), confirmNewPassword)
      );
    }
    return "redirect:/profile";
  }

  /**
   * Return Chats page.
   * @param model to add List of chat to html
   * @return seeker/seekerChatList.html
   */
  @GetMapping("/chats")
  public String getChats(Model model, HttpServletRequest request) {
    long id = (long) request.getSession().getAttribute("id");
    ResponseEntity<List<Chat>> response = restTemplate.exchange(
        "http://finddanus-service-user/" + id + "/chats",
        HttpMethod.GET,
        null,
        new ParameterizedTypeReference<List<Chat>>() {}
    );
    List<Chat> chats = response.getBody();
    model.addAttribute("chats", chats);
    return "seeker/seekerChatList";
  }

  /**
   * Return Seeker Profile page for Seller.
   * @param id of Seeker
   * @return seeker/seekerProfilePreview.html
   */
  @GetMapping("/preview/{id}")
  public String previewSeekerProfile(@PathVariable long id, Model model) {
    Seeker seekerResponse = restTemplate.getForObject(
        "http://finddanus-service-user/" + id,
        Seeker.class
    );
    try {
      model.addAttribute("seeker", seekerResponse);
    } catch (NullPointerException e) {
      model.addAttribute("notFound", true);
    }
    return "seeker/seekerProfilePreview";
  }
}
