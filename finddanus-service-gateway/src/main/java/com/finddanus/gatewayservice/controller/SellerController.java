package com.finddanus.gatewayservice.controller;

import com.finddanus.domain.chat.model.Chat;
import com.finddanus.domain.request.EditPasswordRequest;
import com.finddanus.domain.request.EditProfileRequest;
import com.finddanus.domain.response.ChatListResponse;
import com.finddanus.domain.response.ObserverListResponse;
import com.finddanus.domain.response.ProductListResponse;
import com.finddanus.domain.response.SellerResponse;
import com.finddanus.domain.seeker.model.Observer;
import com.finddanus.domain.seller.model.Product;
import com.finddanus.domain.seller.model.Seller;
import com.finddanus.domain.seller.service.SellerService;
import com.finddanus.gatewayservice.userdetails.FindDanusUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Controller
@RequestMapping(path = "/seller")
public class SellerController {

  @Autowired
  private final SellerService sellerService;

  @Autowired
  private final PasswordEncoder passwordEncoder;

  @Autowired
  private RestTemplate restTemplate;

  public SellerController(SellerService sellerService, PasswordEncoder passwordEncoder) {
    this.sellerService = sellerService;
    this.passwordEncoder = passwordEncoder;
  }

  /**
   * Returns profile of authenticated seller.
   * @param model for the view
   * @param request request data
   * @param auth current authenticated user
   * @return seller/sellerInfo.html
   */
  @GetMapping("/home")
  public String getProductsBySellerId(
      Model model,
      HttpServletRequest request,
      Authentication auth
  ) {
    long id = ((FindDanusUserDetails) auth.getPrincipal()).getId();
    request.getSession().setAttribute("id", id);
    try {
      Seller response = restTemplate.getForObject(
          "http://finddanus-service-user/seller/" + id,
          Seller.class
      );
      ResponseEntity<List<Product>> responseList = restTemplate.exchange(
          "http://finddanus-service-user/seller/" + id + "/products",
          HttpMethod.GET,
          null,
          new ParameterizedTypeReference<List<Product>>() {}
      );
      List<Product> products = responseList.getBody();
      ResponseEntity<List<Observer>> responseFollowers = restTemplate.exchange(
          "http://finddanus-service-user/seller/" + id + "/followers",
          HttpMethod.GET,
          null,
          new ParameterizedTypeReference<List<Observer>>() {}
      );
      List<Observer> followers = responseFollowers.getBody();
      model.addAttribute("products", products);
      model.addAttribute("seekers", followers.size());
      model.addAttribute("seller", response);
    } catch (NullPointerException e) {
      model.addAttribute("notFound", true);
    }
    return "seller/sellerInfo";
  }

  /**
   * Returns edit profile page for seller.
   * @param model for view
   * @param request request data
   * @return seller/sellerEdit if seller exists else redirect to profile page
   */
  @GetMapping("/edit")
  public String updateSellerProfilePage(Model model, HttpServletRequest request) {
    long id = (long) request.getSession().getAttribute("id");
    try {
      Seller response = restTemplate.getForObject(
          "http://finddanus-service-user/seller/" + id,
          Seller.class
      );
      model.addAttribute("seller", response);
      return "seller/sellerEdit";
    } catch (NullPointerException e) {
      return "redirect:/seller/home";
    }
  }

  /**
   * Send data for edit profile.
   * @param sellerId of the seller
   * @param name of seller
   * @param email of seller
   * @param location of seller
   * @param phoneNum of seller
   * @param passwordConfirm by requesting seller
   * @return redirect to profile page if success else edit page
   */
  @PostMapping("/edit")
  public String updateSellerProfile(
      @RequestParam(value = "seller_id") long sellerId,
      @RequestParam(value = "name") String name,
      @RequestParam(value = "email") String email,
      @RequestParam(value = "location") String location,
      @RequestParam(value = "phoneNum") String phoneNum,
      @RequestParam(value = "passwordConfirm") String passwordConfirm
  ) {
    Seller response = restTemplate.getForObject(
        "http://finddanus-service-user/seller/" + sellerId,
        Seller.class
    );
    if (passwordEncoder.matches(passwordConfirm, response.getPassword())) {
      restTemplate.put(
          "http://finddanus-service-user/seller/" + sellerId,
          new EditProfileRequest(name, email, passwordConfirm, location, phoneNum)
      );
    }
    return "redirect:/seller/home";
  }

  /**
   * Send data for change password.
   * @param sellerId of the seller
   * @param oldPassword of seller
   * @param password of seller
   * @param passwordConfirm by seller
   * @return redirect to profile page if success else edit page
   */
  @PostMapping("/editPass")
  public String updateSellerPassword(
      @RequestParam(value = "seller_id") long sellerId,
      @RequestParam(value = "oldPassword") String oldPassword,
      @RequestParam(value = "password") String password,
      @RequestParam(value = "passwordConfirmChange") String passwordConfirm
  ) {
    Seller response = restTemplate.getForObject(
        "http://finddanus-service-user/seller/" + sellerId,
        Seller.class
    );
    if (passwordEncoder.matches(oldPassword, response.getPassword())
        && password.equals(passwordConfirm)) {
      restTemplate.put(
          "http://finddanus-service-user/seller/" + sellerId + "/pass",
          new EditPasswordRequest(oldPassword, passwordEncoder.encode(password), passwordConfirm)
      );
    }
    return "redirect:/seller/home";
  }

  /**
   * Shows seller's followers.
   * @param model for view
   * @param request request data
   * @return seller/sellerFollowers.html page
   */
  @GetMapping("/followers")
  public String getFollowers(Model model, HttpServletRequest request) {
    long id = (long) request.getSession().getAttribute("id");
    try {
      ResponseEntity<List<Observer>> responseFollowers = restTemplate.exchange(
          "http://finddanus-service-user/seller/" + id + "/followers",
          HttpMethod.GET,
          null,
          new ParameterizedTypeReference<List<Observer>>() {}
      );
      List<Observer> followers = responseFollowers.getBody();
      Seller response = restTemplate.getForObject(
          "http://finddanus-service-user/seller/" + id,
          Seller.class
      );
      model.addAttribute("seekers", followers);
      model.addAttribute("seller", response);
    } catch (NullPointerException e) {
      model.addAttribute("noFollowers", true);
    }
    return "seller/sellerFollowers";
  }

  /**
   * Get product page.
   * @param model for view
   * @param request request data
   * @return addProduct.html if success else redirect to profile
   */
  @GetMapping("/addProduct")
  public String getAddProductPage(Model model, HttpServletRequest request) {
    long id = (long) request.getSession().getAttribute("id");
    try {
      Seller response = restTemplate.getForObject(
          "http://finddanus-service-user/seller/" + id,
          Seller.class
      );
      model.addAttribute("seller", response);
      return "seller/addProduct";
    } catch (NullPointerException e) {
      return "redirect:/seller/home";
    }
  }

  /**
   * Add product to database.
   * @param sellerId of product
   * @param title of product
   * @param desc of product
   * @param price of product
   * @return redirect to seller profile
   */
  @PostMapping("/addProduct")
  public String addProduct(
      @RequestParam(value = "seller_id") long sellerId,
      @RequestParam(value = "title") String title,
      @RequestParam(value = "desc") String desc,
      @RequestParam(value = "price") int price
  ) {
    try {
      Seller response = restTemplate.getForObject(
          "http://finddanus-service-user/seller/" + sellerId,
          Seller.class
      );
      ResponseEntity responseAdd = restTemplate.postForObject(
        "http://finddanus-service-user/seller/" + sellerId + "/product",
          new Product(title, desc, price, response),
          ResponseEntity.class
      );
    } catch (NullPointerException e) {
      return "redirect:/seller/home";
    }
    return "redirect:/seller/home";
  }

  /**
   * Remove specified product from database.
   * @param pid of product
   * @return redirect to seller's homepage
   */
  @GetMapping("/removeProduct/{pid}")
  public String removeProduct(
      @PathVariable long pid
  ) {
    restTemplate.delete("http://finddanus-service-user/seller/1/product/" + pid);
    return "redirect:/seller/home";
  }

  /**
   * Show chatlists for currently authenticated seller.
   * @param model for view
   * @param request request data
   * @return seller/sellerChatList.html
   */
  @GetMapping("/chats")
  public String getChats(Model model, HttpServletRequest request) {
    long id = (long) request.getSession().getAttribute("id");
    ResponseEntity<List<Chat>> response = restTemplate.exchange(
        "http://finddanus-service-user/seller/" + id + "/chats",
        HttpMethod.GET,
        null,
        new ParameterizedTypeReference<List<Chat>>() {}
    );
    List<Chat> chats = response.getBody();
    model.addAttribute("chats", chats);
    return "seller/sellerChatList";
  }

  /**
   * Show seller's profile from seeker's view.
   * @param id of seller to be previewed
   * @param model for view
   * @return preview mode of seller profile
   */
  @GetMapping("/preview/{id}")
  public String previewSellerProfile(@PathVariable long id, Model model) {
    try {
      Seller response = restTemplate.getForObject(
          "http://finddanus-service-user/seller/" + id,
          Seller.class
      );
      ResponseEntity<List<Product>> responseList = restTemplate.exchange(
          "http://finddanus-service-user/seller/" + id + "/products",
          HttpMethod.GET,
          null,
          new ParameterizedTypeReference<List<Product>>() {}
      );
      List<Product> products = responseList.getBody();
      ResponseEntity<List<Observer>> responseFollowers = restTemplate.exchange(
          "http://finddanus-service-user/seller/" + id + "/followers",
          HttpMethod.GET,
          null,
          new ParameterizedTypeReference<List<Observer>>() {}
      );
      List<Observer> followers = responseFollowers.getBody();
      model.addAttribute("products", products);
      model.addAttribute("seekers", followers.size());
      model.addAttribute("seller", response);
    } catch (NullPointerException e) {
      model.addAttribute("notFound", true);
    }
    return "seller/sellerInfoPreview";
  }
}
