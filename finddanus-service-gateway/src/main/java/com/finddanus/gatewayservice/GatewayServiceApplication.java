package com.finddanus.gatewayservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;

@ComponentScan("com.finddanus")
@EnableJpaRepositories(basePackages = {
    "com.finddanus.domain.chat.repository",
    "com.finddanus.domain.seeker.repository",
    "com.finddanus.domain.seller.repository"
})
@EntityScan(basePackages = {
    "com.finddanus.domain.chat.model",
    "com.finddanus.domain.seeker.model",
    "com.finddanus.domain.seller.model"
})
@SpringBootApplication
@EnableDiscoveryClient
public class GatewayServiceApplication {

  @Bean
  @LoadBalanced
  public RestTemplate getRestTemplate() {
    return new RestTemplate();
  }

  public static void main(String[] args) {
    SpringApplication.run(GatewayServiceApplication.class, args);
  }
}
