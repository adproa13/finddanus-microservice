package com.finddanus.gatewayservice.register;

import com.finddanus.domain.register.RegisterService;
import com.finddanus.domain.seeker.model.Seeker;
import com.finddanus.domain.seeker.model.SeekerBuilder;
import com.finddanus.domain.seeker.repository.SeekerRepository;
import com.finddanus.domain.seller.model.Seller;
import com.finddanus.domain.seller.model.SellerBuilder;
import com.finddanus.domain.seller.repository.SellerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class RegisterServiceImpl implements RegisterService {

  @Autowired
  private final SeekerRepository seekerRepository;

  @Autowired
  private final SellerRepository sellerRepository;

  private final PasswordEncoder passwordEncoder;

  /**
   * Service that serves register feature.
   * @param seekerRepository of seeker
   * @param sellerRepository of seller
   */
  public RegisterServiceImpl(SeekerRepository seekerRepository,
                             SellerRepository sellerRepository) {
    this.seekerRepository = seekerRepository;
    this.sellerRepository = sellerRepository;
    this.passwordEncoder = new BCryptPasswordEncoder();
  }

  @Override
  public Seeker createSeeker(String name, String email, String location,
                             String phoneNum, String password, String passwordConfirm) {
    if (password.equals(passwordConfirm)) {
      Seeker seeker = SeekerBuilder.createSeeker()
          .withName(name)
          .withEmail(email)
          .withLocation(location)
          .withPhoneNum(phoneNum)
          .withPassword(passwordEncoder.encode(password))
          .withChats(new ArrayList<>())
          .withFollowing(new ArrayList<>())
          .build();
      return seekerRepository.save(seeker);
    }
    return null;
  }

  @Override
  public Seller createSeller(String name, String email, String location,
                             String phoneNum, String password, String passwordConfirm) {
    if (password.equals(passwordConfirm)) {
      Seller seller = SellerBuilder.createSeller()
          .withName(name)
          .withEmail(email)
          .withLocation(location)
          .withPhoneNum(phoneNum)
          .withPassword(passwordEncoder.encode(password))
          .withChats(new ArrayList<>())
          .withFollowers(new ArrayList<>())
          .withProducts(new ArrayList<>())
          .build();
      return sellerRepository.save(seller);
    }
    return null;
  }
}
