package com.finddanus.domain.seeker.repository;

import com.finddanus.domain.seeker.model.Seeker;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SeekerRepository extends JpaRepository<Seeker, Long> {
  Optional<Seeker> findByEmail(String email);
}
