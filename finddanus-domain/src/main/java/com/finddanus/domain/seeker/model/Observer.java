package com.finddanus.domain.seeker.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.finddanus.domain.seller.model.Subject;

@JsonDeserialize(as=Seeker.class)
public interface Observer {
  public void update();

  public void addFollowing(Subject subject);

  public void removeFollowing(Subject subject);

  public long getId();
}
