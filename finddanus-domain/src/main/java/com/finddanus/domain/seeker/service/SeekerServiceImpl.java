package com.finddanus.domain.seeker.service;

import com.finddanus.domain.chat.model.Chat;
import com.finddanus.domain.seeker.model.Seeker;
import com.finddanus.domain.seeker.repository.SeekerRepository;
import com.finddanus.domain.seller.model.Product;
import com.finddanus.domain.seller.model.Seller;
import com.finddanus.domain.seller.model.Subject;
import com.finddanus.domain.seller.repository.ProductRepository;
import com.finddanus.domain.seller.repository.SellerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class SeekerServiceImpl implements SeekerService {

  @Autowired
  private final SeekerRepository seekerRepository;

  @Autowired
  private final SellerRepository sellerRepository;

  @Autowired
  private final ProductRepository productRepository;

  /**
   * Set repository.
   * @param sellerRepository of seller
   * @param seekerRepository of seeker
   * @param productRepository of seller
   */
  public SeekerServiceImpl(
      SeekerRepository seekerRepository,
      SellerRepository sellerRepository,
      ProductRepository productRepository
  ) {
    this.seekerRepository = seekerRepository;
    this.sellerRepository = sellerRepository;
    this.productRepository = productRepository;
  }

  @Override
  public void addSeeker(Seeker seeker) {
    seekerRepository.save(seeker);
  }

  @Override
  public Optional<Seeker> findSeeker(long seekerId) {
    return seekerRepository.findById(seekerId);
  }

  @Override
  public Optional<Seller> findSeller(long sellerId) {
    return sellerRepository.findById(sellerId);
  }

  @Override
  public Seeker getSeekerObjectById(long seekerId) {
    Optional<Seeker> targetSeeker = findSeeker(seekerId);
    try {
      return targetSeeker.get();
    } catch (NoSuchElementException e) {
      return null;
    }
  }

  @Override
  public Seeker editSeekerProfile(
      long seekerId,
      String name,
      String email,
      String location,
      String phoneNumber,
      String passwordConfirm
  ) {
    Seeker thisSeeker = getSeekerObjectById(seekerId);
    if (thisSeeker != null) {
      thisSeeker.setName(name);
      thisSeeker.setEmail(email);
      thisSeeker.setLocation(location);
      thisSeeker.setPhoneNum(phoneNumber);
      return seekerRepository.save(thisSeeker);
    }
    return null;
  }

  @Override
  public Seeker editSeekerPassword(
      long seekerId,
      String oldPassword,
      String newPassword,
      String confirmNewPassword
  ) {
    Seeker thisSeeker = getSeekerObjectById(seekerId);
    if (thisSeeker != null) {
      thisSeeker.setPassword(newPassword);
      return seekerRepository.save(thisSeeker);
    }
    return null;
  }

  @Override
  public Seller getSellerObjectById(long sellerId) {
    Optional<Seller> targetSeller = findSeller(sellerId);
    try {
      return targetSeller.get();
    } catch (NoSuchElementException e) {
      return null;
    }
  }

  @Override
  public void followSeller(long seekerId, long sellerId) {
    Seeker seeker = getSeekerObjectById(seekerId);
    Seller seller = getSellerObjectById(sellerId);
    try {
      seeker.addFollowing(seller);
      seekerRepository.save(seeker);
    } catch (NullPointerException e) {
      return;
    }
  }

  @Override
  public void unfollowSeller(long seekerId, long sellerId) {
    Seeker seeker = getSeekerObjectById(seekerId);
    Seller seller = getSellerObjectById(sellerId);
    try {
      seeker.removeFollowing(seller);
      seekerRepository.save(seeker);
    } catch (NullPointerException e) {
      return;
    }
  }

  @Override
  public List<Subject> showFollowing(long seekerId) {
    Seeker seeker = getSeekerObjectById(seekerId);
    List<Subject> followedSellers = seeker.getFollowing();
    return followedSellers;
  }

  @Override
  public List<Product> showHomepage(long seekerId) {
    Seeker thisSeeker = getSeekerObjectById(seekerId);
    List<Product> homepageProducts = thisSeeker.getProducts();
    return homepageProducts;
  }

  @Override
  public List<Product> showProductByName(String name) {
    List<Product> searchResult = new ArrayList<>();
    for (Product product : productRepository.findAll()) {
      if ((product.getTitle().toLowerCase()).contains(name.toLowerCase())) {
        searchResult.add(product);
      }
    }
    return searchResult;
  }

  @Override
  public List<Product> showProductByLocation(String location) {
    List<Product> searchResult = productRepository.findProductsByLocation(location);
    return searchResult;
  }

  @Override
  public List<Chat> getChats(long seekerId) {
    Optional<Seeker> seeker = findSeeker(seekerId);
    try {
      return seeker.get().getChats();
    } catch (NoSuchElementException e) {
      return null;
    }
  }
}
