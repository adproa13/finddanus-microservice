package com.finddanus.domain.register;

import com.finddanus.domain.seeker.model.Seeker;
import com.finddanus.domain.seller.model.Seller;

public interface RegisterService {
  public Seeker createSeeker(String name, String email, String location,
                             String phoneNum, String password, String passwordConfirm);

  public Seller createSeller(String name, String email, String location,
                             String phoneNum, String password, String passwordConfirm);
}
