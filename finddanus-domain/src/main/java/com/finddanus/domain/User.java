package com.finddanus.domain;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class User {

  @Column(name = "name")
  protected String name;

  @Column(name = "email", nullable = false)
  protected String email;

  @Column(name = "password")
  protected String password;

  @Column(name = "location")
  protected String location;

  @Column(name = "phoneNum")
  protected String phoneNum;

  public void setName(String name) {
    this.name = name;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public void setPhoneNum(String phoneNum) {
    this.phoneNum = phoneNum;
  }

  public String getName() {
    return name;
  }

  public String getEmail() {
    return email;
  }

  public String getPassword() {
    return password;
  }

  public String getLocation() {
    return location;
  }

  public String getPhoneNum() {
    return phoneNum;
  }

  public abstract String getRole();

  public abstract long getId();
}
