package com.finddanus.domain.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.finddanus.domain.seeker.model.Observer;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class ObserverListResponse {

  private ResponseEntity<List<Observer>> response;

  @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
  public ObserverListResponse(@JsonProperty("response") ResponseEntity<List<Observer>> response) {
    this.response = response;
  }

  public ResponseEntity<List<Observer>> getResponse() {
    return response;
  }
}
