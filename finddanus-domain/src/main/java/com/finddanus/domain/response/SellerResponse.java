package com.finddanus.domain.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.finddanus.domain.seller.model.Seller;
import org.springframework.http.ResponseEntity;

public class SellerResponse {

  private ResponseEntity<Seller> response;

  @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
  public SellerResponse(@JsonProperty("response") ResponseEntity<Seller> response) {
    this.response = response;
  }

  public ResponseEntity<Seller> getResponse() {
    return response;
  }
}
