package com.finddanus.domain.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.finddanus.domain.seller.model.Product;
import org.springframework.http.ResponseEntity;

public class ProductResponse {

  private ResponseEntity<Product> response;

  @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
  public ProductResponse(@JsonProperty("response") ResponseEntity<Product> response) {
    this.response = response;
  }

  public ResponseEntity<Product> getResponse() {
    return response;
  }
}
