package com.finddanus.domain.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.finddanus.domain.seller.model.Subject;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class SubjectListResponse {

  private ResponseEntity<List<Subject>> response;

  @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
  public SubjectListResponse(@JsonProperty("response") ResponseEntity<List<Subject>> response) {
    this.response = response;
  }

  public ResponseEntity<List<Subject>> getResponse() {
    return response;
  }
}
