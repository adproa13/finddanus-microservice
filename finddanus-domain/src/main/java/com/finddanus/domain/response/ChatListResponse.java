package com.finddanus.domain.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.finddanus.domain.chat.model.Chat;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class ChatListResponse {

  private ResponseEntity<List<Chat>> response;

  @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
  public ChatListResponse(@JsonProperty("response") ResponseEntity<List<Chat>> response) {
    this.response = response;
  }

  public ResponseEntity<List<Chat>> getResponse() {
    return response;
  }
}
