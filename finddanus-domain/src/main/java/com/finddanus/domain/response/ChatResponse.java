package com.finddanus.domain.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.finddanus.domain.chat.model.Chat;
import org.springframework.http.ResponseEntity;

public class ChatResponse {

  private ResponseEntity<Chat> response;

  @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
  public ChatResponse(@JsonProperty("response") ResponseEntity<Chat> response) {
    this.response = response;
  }

  public ResponseEntity<Chat> getResponse() {
    return response;
  }
}
