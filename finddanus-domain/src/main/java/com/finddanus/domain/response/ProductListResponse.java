package com.finddanus.domain.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.finddanus.domain.seller.model.Product;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class ProductListResponse {

  private ResponseEntity<List<Product>> response;

  @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
  public ProductListResponse(@JsonProperty("response") ResponseEntity<List<Product>> response) {
    this.response = response;
  }

  public ResponseEntity<List<Product>> getResponse() {
    return response;
  }
}
