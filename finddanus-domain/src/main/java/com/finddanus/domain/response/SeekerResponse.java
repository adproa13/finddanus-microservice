package com.finddanus.domain.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.finddanus.domain.seeker.model.Seeker;
import org.springframework.http.ResponseEntity;

public class SeekerResponse {

  private ResponseEntity<Seeker> response;

  @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
  public SeekerResponse(@JsonProperty("response") ResponseEntity<Seeker> response) {
    this.response = response;
  }

  public ResponseEntity<Seeker> getResponse() {
    return response;
  }
}
