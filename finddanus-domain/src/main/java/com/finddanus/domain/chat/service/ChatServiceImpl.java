package com.finddanus.domain.chat.service;

import com.finddanus.domain.chat.model.Chat;
import com.finddanus.domain.chat.repository.ChatRepository;
import com.finddanus.domain.chat.repository.MessageRepository;
import com.finddanus.domain.seeker.model.Seeker;
import com.finddanus.domain.seeker.service.SeekerService;
import com.finddanus.domain.seller.model.Seller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class ChatServiceImpl implements ChatService {

  @Autowired
  private ChatRepository chatRepository;

  @Autowired
  private MessageRepository messageRepository;

  @Autowired
  private SeekerService seekerService;

  /**
   * Service that serves chat.
   * @param chatRepository of chat
   * @param messageRepository of message
   * @param seekerService for seeker and seller
   */
  public ChatServiceImpl(ChatRepository chatRepository,
                         MessageRepository messageRepository,
                         SeekerService seekerService) {
    this.chatRepository = chatRepository;
    this.messageRepository = messageRepository;
    this.seekerService = seekerService;
  }

  @Override
  public Optional<Chat> findChatById(long chatId) {
    return chatRepository.findById(chatId);
  }

  @Override
  public Chat addSeekerMessage(long chatId, long seekerId, String message) {
    Seeker seeker = seekerService.getSeekerObjectById(seekerId);
    Optional<Chat> chatOptional = findChatById(chatId);
    try {
      Chat chat = chatOptional.get();
      chat.addMessage(seeker, message);
      return chatRepository.save(chat);
    } catch (NoSuchElementException e) {
      return null;
    }
  }

  @Override
  public Chat addSellerMessage(long chatId, long sellerId, String message) {
    Seller seller = seekerService.getSellerObjectById(sellerId);
    Optional<Chat> chatOptional = findChatById(chatId);
    try {
      Chat chat = chatOptional.get();
      chat.addMessage(seller, message);
      return chatRepository.save(chat);
    } catch (NoSuchElementException e) {
      return null;
    }
  }
}
