package com.finddanus.domain.chat.model;

import com.finddanus.domain.seller.model.Seller;

public class SellerMessageGenerator extends MessageGenerator {

  private Seller seller;

  public SellerMessageGenerator(Seller s) {
    this.seller = s;
  }

  @Override
  public Message createMessage(String text) {
    Message message = new Message(this.seller, text);
    return message;
  }
}
