package com.finddanus.domain.chat.model;

import com.finddanus.domain.User;
import com.finddanus.domain.seeker.model.Seeker;
import com.finddanus.domain.seller.model.Seller;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "message")
public class Message {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "message_id", nullable = false, updatable = false)
  private long id;

  @Column(name = "content")
  private String content;

  @ManyToOne
  private Seeker seeker;

  @ManyToOne
  private Seller seller;

  /**
   * Constructor of message.
   * @param sender of message
   * @param text content of message
   */
  public Message(User sender, String text) {
    try {
      this.seeker = (Seeker) sender;
    } catch (ClassCastException e) {
      this.seller = (Seller) sender;
    }
    this.content = text;
  }

  public Message() {}

  public String getContent() {
    return content;
  }

  public Seeker getSeeker() {
    return seeker;
  }

  public Seller getSeller() {
    return seller;
  }

  public long getId() {
    return id;
  }
}
