package com.finddanus.domain.chat.repository;

import com.finddanus.domain.chat.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<Message, Long> {
}
