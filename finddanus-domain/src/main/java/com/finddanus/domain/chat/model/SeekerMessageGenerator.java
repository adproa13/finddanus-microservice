package com.finddanus.domain.chat.model;

import com.finddanus.domain.seeker.model.Seeker;

public class SeekerMessageGenerator extends MessageGenerator {

  private Seeker seeker;

  public SeekerMessageGenerator(Seeker s) {
    this.seeker = s;
  }

  @Override
  public Message createMessage(String text) {
    Message message = new Message(this.seeker, text);
    return message;
  }
}
