package com.finddanus.domain.chat.model;

public abstract class MessageGenerator {
  public abstract Message createMessage(String text);
}
