package com.finddanus.domain.chat.model;

import com.finddanus.domain.User;
import com.finddanus.domain.seeker.model.Seeker;
import com.finddanus.domain.seller.model.Seller;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "chat")
public class Chat {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "chat_id", nullable = false, updatable = false)
  private long id;

  @JoinColumn(name = "seeker_id")
  @ManyToOne(
      cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH, CascadeType.REMOVE},
      targetEntity = Seeker.class
  )
  private Seeker seeker;

  @JoinColumn(name = "seller_id")
  @ManyToOne(
      cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH, CascadeType.REMOVE},
      targetEntity = Seller.class
  )
  private Seller seller;

  @OneToMany(
      cascade = CascadeType.ALL
  )
  private List<Message> messageList;

  @Transient
  private MessageGenerator messageGenerator;

  /**
   * Create user who will sell products.
   *
   * @param sk accepts seeker object from Seeker
   * @param sl accepts seller object from Seller
   */
  public Chat(Seeker sk, Seller sl) {
    this.seeker = sk;
    this.seller = sl;
    this.messageList = new ArrayList<>();

  }

  public Chat() {
  }

  /**
   * Create user who will sell products.
   *
   * @param sender accepts seeker/seller object from Seeker/Seller respectively
   */
  public void addMessage(User sender, String text) {
    String identifier = sender.getEmail();
    if (identifier.equals(this.seeker.getEmail())) {
      this.messageGenerator = new SeekerMessageGenerator(this.seeker);
      Message message = this.messageGenerator.createMessage(text);
      this.messageList.add(message);
    } else if (identifier.equals(this.seller.getEmail())) {
      this.messageGenerator = new SellerMessageGenerator(this.seller);
      Message message = this.messageGenerator.createMessage(text);
      this.messageList.add(message);
    }
  }

  public List<Message> getMessageList() {
    return messageList;
  }

  public Seeker getSeeker() {
    return seeker;
  }

  public Seller getSeller() {
    return seller;
  }

  public long getId() {
    return id;
  }

  public void setId(long newId) {
    this.id = newId;
  }

}
