package com.finddanus.domain.request;

public class ChatRequest {

  private long chatId;
  private long userId;
  private String message;

  public ChatRequest(long chatId, long userId, String message) {
    this.chatId = chatId;
    this.userId = userId;
    this.message = message;
  }

  public long getChatId() {
    return chatId;
  }

  public long getUserId() {
    return userId;
  }

  public String getMessage() {
    return message;
  }
}
