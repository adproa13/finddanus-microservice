package com.finddanus.domain.request;

public class FollowUnfollowRequest {

  private long seekerId;
  private long sellerId;

  public FollowUnfollowRequest(long seekerId, long sellerId) {
    this.seekerId = seekerId;
    this.sellerId = sellerId;
  }

  public long getSeekerId() {
    return seekerId;
  }

  public long getSellerId() {
    return sellerId;
  }
}
