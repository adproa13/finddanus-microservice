package com.finddanus.domain.request;

import com.finddanus.domain.seller.model.Seller;

public class ProductRequest {

  private String title;
  private String desc;
  private int price;
  private long sellerId;

  public ProductRequest(String title, String desc, int price, long sellerId) {
    this.title = title;
    this.desc = desc;
    this.price = price;
    this.sellerId = sellerId;
  }

  public String getTitle() {
    return title;
  }

  public String getDesc() {
    return desc;
  }

  public int getPrice() {
    return price;
  }

  public long getSellerId() {
    return sellerId;
  }
}
