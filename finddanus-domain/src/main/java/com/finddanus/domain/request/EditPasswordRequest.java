package com.finddanus.domain.request;

public class EditPasswordRequest {

  private String oldPassword;
  private String password;
  private String passwordConfirm;

  public EditPasswordRequest(String oldPassword, String password, String passwordConfirm) {
    this.oldPassword = oldPassword;
    this.password = password;
    this.passwordConfirm = passwordConfirm;
  }

  public String getOldPassword() {
    return oldPassword;
  }

  public String getPassword() {
    return password;
  }

  public String getPasswordConfirm() {
    return passwordConfirm;
  }
}
