package com.finddanus.domain.request;

public class EditProfileRequest {

  private String name;
  private String email;
  private String password;
  private String location;
  private String phoneNum;

  public EditProfileRequest(String name, String email, String password, String location, String phoneNum) {
    this.name = name;
    this.email = email;
    this.password = password;
    this.location = location;
    this.phoneNum = phoneNum;
  }

  public String getName() {
    return name;
  }

  public String getEmail() {
    return email;
  }

  public String getPassword() {
    return password;
  }

  public String getLocation() {
    return location;
  }

  public String getPhoneNum() {
    return phoneNum;
  }
}
