package com.finddanus.domain.seller.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.finddanus.domain.seeker.model.Seeker;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "product")
@JsonIdentityInfo(generator= ObjectIdGenerators.IntSequenceGenerator.class, property="@productid")
public class Product {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "p_id", nullable = false, updatable = false)
  private long id;

  @Size(min = 1, max = 100)
  @Column(name = "p_title")
  private String title;

  @Size(min = 10, max = 500)
  @Column(name = "p_desc")
  private String description;

  @Column(name = "p_price")
  private int price;

  @JoinColumn(name = "seller_id")
  @ManyToOne(
      fetch = FetchType.EAGER,
      targetEntity = Seller.class
  )
  private Seller seller;

  @ManyToMany(
      mappedBy = "homepage",
      cascade = CascadeType.ALL,
      targetEntity = Seeker.class
  )
  private List<Seeker> seekers;

  /**
   * Create an entity that will be sold by seller.
   * @param id of product (unique)
   * @param title of product
   * @param description of product
   * @param price of product
   * @param seller of product
   */
  public Product(long id, String title, String description, int price, Seller seller) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.price = price;
    this.seller = seller;
  }

  /**
   * Create an entity that will be sold by seller.
   * @param title of product
   * @param description of product
   * @param price of product
   * @param seller of product
   */
  public Product(String title, String description, int price, Seller seller) {
    this.title = title;
    this.description = description;
    this.price = price;
    this.seller = seller;
  }

  public Product() {}

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public Seller getSeller() {
    return seller;
  }

  public void setSeller(Seller seller) {
    this.seller = seller;
  }
}
