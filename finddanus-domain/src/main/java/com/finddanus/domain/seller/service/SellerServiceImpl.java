package com.finddanus.domain.seller.service;

import com.finddanus.domain.chat.model.Chat;
import com.finddanus.domain.seeker.model.Observer;
import com.finddanus.domain.seller.model.Product;
import com.finddanus.domain.seller.model.Seller;
import com.finddanus.domain.seller.repository.ProductRepository;
import com.finddanus.domain.seller.repository.SellerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class SellerServiceImpl implements SellerService {

  @Autowired
  private final SellerRepository sellerRepository;

  @Autowired
  private final ProductRepository productRepository;

  /**
   * Service that serves logic business of seller.
   * @param sellerRepository of seller
   * @param productRepository of product
   */
  public SellerServiceImpl(SellerRepository sellerRepository,
                           ProductRepository productRepository) {
    this.sellerRepository = sellerRepository;
    this.productRepository = productRepository;
  }

  @Override
  public List<Seller> findAllSellers() {
    return sellerRepository.findAll();
  }

  @Override
  public Optional<Seller> findSeller(long id) {
    return sellerRepository.findById(id);
  }

  @Override
  public void removeSeller(long id) {
    sellerRepository.deleteById(id);
  }

  @Override
  public Seller addSeller(Seller seller) {
    return sellerRepository.save(seller);
  }

  @Override
  public Seller editProfile(long id, String name, String email,
                            String location, String phoneNum, String passwordConfirm) {
    Optional<Seller> sellerIfExist = findSeller(id);
    if (sellerIfExist.isPresent()) {
      Seller seller = sellerIfExist.get();
      seller.setName(name);
      seller.setEmail(email);
      seller.setLocation(location);
      seller.setPhoneNum(phoneNum);
      return sellerRepository.save(seller);
    }
    return null;
  }

  @Override
  public Seller editPassword(long id, String oldPassword, String password, String passwordConfirm) {
    Optional<Seller> sellerIfExist = findSeller(id);
    if (sellerIfExist.isPresent()) {
      Seller seller = sellerIfExist.get();
      seller.setPassword(password);
      return sellerRepository.save(seller);
    }
    return null;
  }

  @Override
  public List<Product> findAllProducts() {
    return productRepository.findAll();
  }

  @Override
  public List<Product> findProductsBySellerId(long id) {
    return productRepository.findProductsBySellerId(id);
  }

  @Override
  public Optional<Product> findProduct(long id) {
    return productRepository.findById(id);
  }

  @Override
  public void removeProduct(long id) {
    Optional<Product> productIfExist = findProduct(id);
    if (productIfExist.isPresent()) {
      Product product = productIfExist.get();
      Seller seller = (Seller) product.getSeller();
      seller.removeProduct(product);
      sellerRepository.save(seller);
      productRepository.deleteById(id);
    }
  }

  @Override
  public Product addProduct(Product product) {
    Seller seller = (Seller) product.getSeller();
    seller.addProduct(product);
    sellerRepository.save(seller);
    return product;
  }

  @Override
  public Product editProduct(Product product) {
    return productRepository.save(product);
  }

  @Override
  public List<Observer> getFollowers(long sellerId) {
    Optional<Seller> seller = findSeller(sellerId);
    try {
      return seller.get().getFollowers();
    } catch (NoSuchElementException e) {
      return null;
    }
  }

  @Override
  public List<Chat> getChats(long sellerId) {
    Optional<Seller> seller = findSeller(sellerId);
    try {
      return seller.get().getChats();
    } catch (NoSuchElementException e) {
      return null;
    }
  }
}
