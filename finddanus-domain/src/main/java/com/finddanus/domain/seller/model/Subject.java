package com.finddanus.domain.seller.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.finddanus.domain.seeker.model.Observer;

import java.util.List;

@JsonDeserialize(as=Seller.class)
public interface Subject {
  public void addFollower(Observer observer);

  public void removeFollower(Observer observer);

  public void notifyFollowers();

  public List<Product> getProducts();

  public void addProduct(Product product);

  public void removeProduct(Product product);

  public long getId();
}
