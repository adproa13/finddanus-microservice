package com.finddanus.domain.seller.model;

import com.finddanus.domain.chat.model.Chat;
import com.finddanus.domain.seeker.model.Observer;

import java.util.List;

public final class SellerBuilder {

  private long id;
  private String name;
  private String email;
  private String password;
  private String location;
  private String phoneNum;
  private List<Chat> chats;
  private List<Observer> followers;
  private List<Product> products;

  private SellerBuilder() {
  }

  public static SellerBuilder createSeller() {
    return new SellerBuilder();
  }

  public SellerBuilder withId(long id) {
    this.id = id;
    return this;
  }

  public SellerBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public SellerBuilder withEmail(String email) {
    this.email = email;
    return this;
  }

  public SellerBuilder withPassword(String password) {
    this.password = password;
    return this;
  }

  public SellerBuilder withLocation(String location) {
    this.location = location;
    return this;
  }

  public SellerBuilder withPhoneNum(String phoneNum) {
    this.phoneNum = phoneNum;
    return this;
  }

  public SellerBuilder withChats(List<Chat> chats) {
    this.chats = chats;
    return this;
  }

  public SellerBuilder withFollowers(List<Observer> followers) {
    this.followers = followers;
    return this;
  }

  public SellerBuilder withProducts(List<Product> products) {
    this.products = products;
    return this;
  }

  public Seller build() {
    return new Seller(this);
  }

  public long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getEmail() {
    return email;
  }

  public String getPassword() {
    return password;
  }

  public String getLocation() {
    return location;
  }

  public String getPhoneNum() {
    return phoneNum;
  }

  public List<Chat> getChats() {
    return chats;
  }

  public List<Observer> getFollowers() {
    return followers;
  }

  public List<Product> getProducts() {
    return products;
  }
}
