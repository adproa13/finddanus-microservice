package com.finddanus.domain.seller.repository;

import com.finddanus.domain.seller.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {

  @Query(
      value = "SELECT * FROM PRODUCT WHERE seller_id = ?1",
      nativeQuery = true
  )
  List<Product> findProductsBySellerId(long sellerId);

  @Query(
      value = "SELECT * FROM PRODUCT P JOIN SELLER S "
          + "ON P.seller_id = S.seller_id WHERE S.location = ?1",
      nativeQuery = true
  )
  List<Product> findProductsByLocation(String location);
}
