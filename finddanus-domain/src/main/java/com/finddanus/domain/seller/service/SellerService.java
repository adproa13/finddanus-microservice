package com.finddanus.domain.seller.service;

import com.finddanus.domain.chat.model.Chat;
import com.finddanus.domain.seeker.model.Observer;
import com.finddanus.domain.seller.model.Product;
import com.finddanus.domain.seller.model.Seller;

import java.util.List;
import java.util.Optional;

public interface SellerService {
  public List<Seller> findAllSellers();

  public Optional<Seller> findSeller(long id);

  public void removeSeller(long id);

  public Seller addSeller(Seller seller);

  public Seller editProfile(long id, String name, String email,
                            String location, String phoneNum, String passwordConfirm);

  public Seller editPassword(long id, String oldPassword,
                             String password, String passwordConfirm);

  public List<Product> findAllProducts();

  public List<Product> findProductsBySellerId(long id);

  public Optional<Product> findProduct(long id);

  public void removeProduct(long id);

  public Product addProduct(Product product);

  public Product editProduct(Product product);

  public List<Observer> getFollowers(long sellerId);

  public List<Chat> getChats(long sellerId);
}
