package com.finddanus.seller.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.finddanus.domain.seller.model.Product;
import com.finddanus.domain.seller.model.Seller;
import com.finddanus.domain.seller.model.SellerBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ProductTest {

  private Product product;
  private Product product2;
  private Seller seller;

  /**
   * init data.
   */
  @BeforeEach
  public void setUp() {
    seller = SellerBuilder.createSeller()
        .withId(1)
        .withName("Michael")
        .withEmail("michael@gmail.com")
        .withPassword("12345678")
        .withLocation("Jakarta")
        .withPhoneNum("081234567890")
        .build();
    product = new Product(1, "dummyTitle", "dummyDescription", 1000, seller);
    product2 = new Product("dummyTitle2", "dummyDescription2", 1000, seller);
  }

  @Test
  public void testGetId() {
    assertEquals(1, product.getId());
  }

  @Test
  public void testSetId() {
    product.setId(2);
    assertEquals(2, product.getId());
  }

  @Test
  public void testGetTitle() {
    assertEquals("dummyTitle", product.getTitle());
  }

  @Test
  public void testSetTitle() {
    product.setTitle("title");
    assertEquals("title", product.getTitle());
  }

  @Test
  public void testGetDescription() {
    assertEquals("dummyDescription", product.getDescription());
  }

  @Test
  public void testSetDescription() {
    product.setDescription("description");
    assertEquals("description", product.getDescription());
  }

  @Test
  public void testGetPrice() {
    assertEquals(1000, product.getPrice());
  }

  @Test
  public void testSetPrice() {
    product.setPrice(2000);
    assertEquals(2000, product.getPrice());
  }

  @Test
  public void testGetSeller() {
    assertEquals(seller, product.getSeller());
  }

  @Test
  public void testSetSeller() {
    Seller sellerNew = SellerBuilder.createSeller()
        .withId(2)
        .withName("Bambang")
        .withEmail("bambang@gmail.com")
        .withPassword("12345678")
        .withLocation("Jakarta")
        .withPhoneNum("081234567890")
        .build();
    product.setSeller(sellerNew);
    assertEquals(sellerNew, product.getSeller());
  }
}
