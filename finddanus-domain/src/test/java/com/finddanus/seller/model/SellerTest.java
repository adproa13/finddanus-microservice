package com.finddanus.seller.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import com.finddanus.domain.chat.model.Chat;
import com.finddanus.domain.seeker.model.Observer;
import com.finddanus.domain.seeker.model.Seeker;
import com.finddanus.domain.seeker.model.SeekerBuilder;
import com.finddanus.domain.seller.model.Product;
import com.finddanus.domain.seller.model.Seller;
import com.finddanus.domain.seller.model.SellerBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SellerTest {

  private Seller seller;
  private Seeker seeker;
  private Product product;
  private List<Chat> chats;
  private List<Observer> followers;
  private List<Product> products;

  /**
   * init data.
   */
  @BeforeEach
  public void setUp() {
    chats = new ArrayList<>();
    followers = new ArrayList<>();
    products = new ArrayList<>();
    seller = SellerBuilder.createSeller()
        .withId(1)
        .withName("Michael")
        .withEmail("michael@gmail.com")
        .withPassword("12345678")
        .withLocation("Jakarta")
        .withPhoneNum("081234567890")
        .withChats(chats)
        .withFollowers(followers)
        .withProducts(products)
        .build();
    seeker = SeekerBuilder.createSeeker()
        .withId(1)
        .withName("Rifqi")
        .withEmail("rifqi@gmail.com")
        .withPassword("cobaCoba0808")
        .withLocation("Bogor")
        .withPhoneNum("080808080808")
        .withChats(chats)
        .withFollowing(new ArrayList<>())
        .build();
    product = new Product(1, "risol", "risol enak", 1000, seller);
  }

  @Test
  public void testGetId() {
    assertEquals(1, seller.getId());
  }


  @Test
  public void testGetName() {
    assertEquals("Michael", seller.getName());
  }

  @Test
  public void testSetName() {
    seller.setName("Bambang");
    assertEquals("Bambang", seller.getName());
  }

  @Test
  public void testGetEmail() {
    assertEquals("michael@gmail.com", seller.getEmail());
  }

  @Test
  public void testSetEmail() {
    seller.setEmail("bambang@gmail.com");
    assertEquals("bambang@gmail.com", seller.getEmail());
  }

  @Test
  public void testGetPassword() {
    assertEquals("12345678", seller.getPassword());
  }

  @Test
  public void testSetPassword() {
    seller.setPassword("bambangganteng123");
    assertEquals("bambangganteng123", seller.getPassword());
  }

  @Test
  public void testGetLocation() {
    assertEquals("Jakarta", seller.getLocation());
  }

  @Test
  public void testSetLocation() {
    seller.setLocation("Bandung");
    assertEquals("Bandung", seller.getLocation());
  }

  @Test
  public void testGetPhoneNum() {
    assertEquals("081234567890", seller.getPhoneNum());
  }

  @Test
  public void testSetPhoneNum() {
    seller.setPhoneNum("081211223344");
    assertEquals("081211223344", seller.getPhoneNum());
  }

  @Test
  public void testGetChats() {
    assertEquals(chats, seller.getChats());
  }

  @Test
  public void testGetFollowers() {
    assertEquals(followers, seller.getFollowers());
  }

  @Test
  public void testGetProducts() {
    assertEquals(products, seller.getProducts());
  }

  @Test
  public void testAddFollower() {
    assertEquals(0, seller.getFollowers().size());
    seller.addFollower(seeker);
    assertEquals(1, seller.getFollowers().size());
  }

  @Test
  public void testRemoveFollower() {
    assertEquals(0, seller.getFollowers().size());
    seller.addFollower(seeker);
    assertEquals(1, seller.getFollowers().size());
    seller.removeFollower(seeker);
    assertEquals(0, seller.getFollowers().size());
  }

  @Test
  public void testAddProduct() {
    seller.addFollower(seeker);
    assertEquals(0, seller.getProducts().size());
    seller.addProduct(product);
    assertEquals(1, seller.getProducts().size());
  }

  @Test
  public void testRemoveProduct() {
    seller.addFollower(seeker);
    assertEquals(0, seller.getProducts().size());
    seller.addProduct(product);
    assertEquals(1, seller.getProducts().size());
    seller.removeProduct(product);
    assertEquals(0, seller.getProducts().size());
  }

  @Test
  public void testAddChat() {
    seller.addChat(new Chat(seeker, seller));
    assertEquals(1, seller.getChats().size());
  }

  @Test
  public void testRemoveChat() {
    Chat chat = new Chat(seeker, seller);
    seller.addChat(chat);
    assertEquals(1, seller.getChats().size());
    seller.removeChat(chat);
    assertEquals(0, seller.getChats().size());
  }

  @Test
  public void testGetRole() {
    assertEquals("ROLE_SELLER", seller.getRole());
  }
}
