package com.finddanus.userservice.handler;

import com.finddanus.domain.chat.model.Chat;
import com.finddanus.domain.request.EditPasswordRequest;
import com.finddanus.domain.request.EditProfileRequest;
import com.finddanus.domain.seeker.model.Observer;
import com.finddanus.domain.seller.model.Product;
import com.finddanus.domain.seller.model.Seller;
import com.finddanus.domain.seller.service.SellerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin("*")
@RestController
@RequestMapping("/seller")
public class SellerHandler {

  @Autowired
  private SellerService sellerService;

  public SellerHandler(SellerService sellerService) {
    this.sellerService = sellerService;
  }

  @CrossOrigin
  @GetMapping
  public ResponseEntity<List<Seller>> findAll() {
    return new ResponseEntity<>(sellerService.findAllSellers(), HttpStatus.OK);
  }

  @GetMapping("/{id}")
  public ResponseEntity<Seller> findById(@PathVariable long id) {
    try {
      return new ResponseEntity<>(sellerService.findSeller(id).get(), HttpStatus.OK);
    } catch (NoSuchElementException e) {
      return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }
  }

  @PutMapping("/{id}")
  public ResponseEntity editProfile(@PathVariable long id, @RequestBody EditProfileRequest request) {
    Seller edit = sellerService.editProfile(
        id,
        request.getName(),
        request.getEmail(),
        request.getLocation(),
        request.getPhoneNum(),
        request.getPassword()
    );
    return new ResponseEntity(HttpStatus.OK);
  }

  @PutMapping("/{id}/pass")
  public ResponseEntity editPassword(@PathVariable long id, @RequestBody EditPasswordRequest request) {
    Seller edit = sellerService.editPassword(
        id,
        request.getOldPassword(),
        request.getPassword(),
        request.getPasswordConfirm()
    );
    return new ResponseEntity(HttpStatus.OK);
  }

  @GetMapping("/{id}/products")
  public ResponseEntity<List<Product>> getProducts(@PathVariable long id) {
    return new ResponseEntity<>(sellerService.findProductsBySellerId(id), HttpStatus.OK);
  }

  @GetMapping("/{id}/product/{pid}")
  public ResponseEntity<Product> getProduct(@PathVariable long id, @PathVariable long pid) {
    try {
      return new ResponseEntity<>(sellerService.findProduct(pid).get(), HttpStatus.OK);
    } catch (NoSuchElementException e) {
      return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }
  }

  @DeleteMapping("{id}/product/{pid}")
  public ResponseEntity removeProduct(@PathVariable long id, @PathVariable long pid) {
    sellerService.removeProduct(pid);
    return new ResponseEntity(HttpStatus.OK);
  }

  @PostMapping("{id}/product")
  public ResponseEntity addProduct(@PathVariable long id, @RequestBody Product product) {
    Product add = sellerService.addProduct(product);
    return new ResponseEntity(HttpStatus.OK);
  }

  @GetMapping("{id}/followers")
  public ResponseEntity<List<Observer>> getFollowers(@PathVariable long id) {
    return new ResponseEntity<>(sellerService.getFollowers(id), HttpStatus.OK);
  }

  @GetMapping("{id}/chats")
  public ResponseEntity<List<Chat>> getChats(@PathVariable long id) {
    return new ResponseEntity<>(sellerService.getChats(id), HttpStatus.OK);
  }
}
