package com.finddanus.userservice.handler;

import com.finddanus.domain.chat.model.Chat;
import com.finddanus.domain.request.EditPasswordRequest;
import com.finddanus.domain.request.EditProfileRequest;
import com.finddanus.domain.request.FollowUnfollowRequest;
import com.finddanus.domain.seeker.model.Seeker;
import com.finddanus.domain.seeker.service.SeekerService;
import com.finddanus.domain.seller.model.Product;
import com.finddanus.domain.seller.model.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin("*")
@RestController
public class SeekerHandler {

  @Autowired
  private SeekerService seekerService;

  public SeekerHandler(SeekerService seekerService) {
    this.seekerService = seekerService;
  }

  @CrossOrigin
  @GetMapping("/")
  public ResponseEntity landing() {
    return new ResponseEntity(HttpStatus.OK);
  }

  @GetMapping("/{id}")
  public ResponseEntity<Seeker> findById(@PathVariable long id) {
    try {
      return new ResponseEntity<>(seekerService.findSeeker(id).get(), HttpStatus.OK);
    } catch (NoSuchElementException e) {
      return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }
  }

  @PutMapping("/{id}")
  public ResponseEntity editProfile(@PathVariable long id, @RequestBody EditProfileRequest request) {
    Seeker edit = seekerService.editSeekerProfile(
        id,
        request.getName(),
        request.getEmail(),
        request.getLocation(),
        request.getPhoneNum(),
        request.getPassword()
    );
    return new ResponseEntity(HttpStatus.OK);
  }

  @PutMapping("/{id}/pass")
  public ResponseEntity editPassword(@PathVariable long id, @RequestBody EditPasswordRequest request) {
    Seeker edit = seekerService.editSeekerPassword(
        id,
        request.getOldPassword(),
        request.getPassword(),
        request.getPasswordConfirm()
    );
    return new ResponseEntity(HttpStatus.OK);
  }

  @CrossOrigin
  @PostMapping("{id}/follow")
  public ResponseEntity followSeller(@PathVariable long id, @RequestBody FollowUnfollowRequest request) {
    seekerService.followSeller(request.getSeekerId(), request.getSellerId());
    return new ResponseEntity(HttpStatus.OK);
  }

  @CrossOrigin
  @PostMapping("{id}/unfollow")
  public ResponseEntity unfollowSeller(@PathVariable long id, @RequestBody FollowUnfollowRequest request) {
    seekerService.unfollowSeller(request.getSeekerId(), request.getSellerId());
    return new ResponseEntity(HttpStatus.OK);
  }

  @CrossOrigin
  @GetMapping("{id}/product={name}")
  public ResponseEntity<List<Product>> getProductsByName(@PathVariable long id, @PathVariable String name) {
    return new ResponseEntity<>(seekerService.showProductByName(name), HttpStatus.OK);
  }

  @GetMapping("{id}/location={location}")
  public ResponseEntity<List<Product>> getProductsByLocation(@PathVariable long id, @PathVariable String location) {
    return new ResponseEntity<>(seekerService.showProductByLocation(location), HttpStatus.OK);
  }

  @GetMapping("{id}/home")
  public ResponseEntity<List<Product>> getHomepage(@PathVariable long id) {
    return new ResponseEntity<>(seekerService.showHomepage(id), HttpStatus.OK);
  }

  @GetMapping("{id}/following")
  public ResponseEntity<List<Subject>> getFollowing(@PathVariable long id) {
    return new ResponseEntity<>(seekerService.showFollowing(id), HttpStatus.OK);
  }

  @GetMapping("{id}/chats")
  public ResponseEntity<List<Chat>> getChats(@PathVariable long id) {
    return new ResponseEntity<>(seekerService.getChats(id), HttpStatus.OK);
  }
}
