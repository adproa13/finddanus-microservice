package com.finddanus.chatservice.handler;

import com.finddanus.domain.chat.model.Chat;
import com.finddanus.domain.chat.service.ChatService;
import com.finddanus.domain.request.ChatRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.NoSuchElementException;

@RestController
public class ChatHandler {

  @Autowired
  private ChatService chatService;

  @GetMapping("/chat/{chatId}")
  public ResponseEntity<Chat> getChatById(@PathVariable long chatId) {
    try {
      return new ResponseEntity<>(chatService.findChatById(chatId).get(), HttpStatus.OK);
    } catch (NoSuchElementException e) {
      return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }
  }

  @PostMapping("/chat/send")
  public ResponseEntity<Chat> sendSeekerMessage(@RequestBody ChatRequest request) {
    Chat sent = chatService.addSeekerMessage(
        request.getChatId(),
        request.getUserId(),
        request.getMessage());
    return new ResponseEntity<>(sent, HttpStatus.OK);
  }

  @PostMapping("/seller/chat/send")
  public ResponseEntity<Chat> sendSellerMessage(@RequestBody ChatRequest request) {
    Chat sent = chatService.addSellerMessage(
        request.getChatId(),
        request.getUserId(),
        request.getMessage());
    return new ResponseEntity<>(sent, HttpStatus.OK);
  }
}
