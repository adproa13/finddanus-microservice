package com.finddanus.seeker.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.finddanus.chat.model.Chat;
import com.finddanus.seller.model.Product;
import com.finddanus.seller.model.Seller;
import com.finddanus.seller.model.SellerBuilder;
import com.finddanus.seller.model.Subject;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class SeekerTest {
  private Seeker seeker;
  private Seller seller;
  private List<Chat> chats;
  private List<Subject> following;
  private List<Observer> followers;
  private List<Product> products;

  /**
   * set up test.
   */
  @BeforeEach
  public void setUp() {
    chats = new ArrayList<>();
    following = new ArrayList<>();
    followers = new ArrayList<>();
    products = new ArrayList<>();
    seeker = new Seeker();
    seeker = SeekerBuilder.createSeeker()
        .withId(1)
        .withName("Rifqi")
        .withEmail("rifqi@gmail.com")
        .withPassword("cobaCoba0808")
        .withLocation("Bogor")
        .withPhoneNum("080808080808")
        .withChats(chats)
        .withFollowing(following)
        .build();

    seller = SellerBuilder.createSeller()
        .withId(1)
        .withName("Michael")
        .withEmail("michael@gmail.com")
        .withPassword("12345678")
        .withLocation("Jakarta")
        .withPhoneNum("081234567890")
        .withChats(chats)
        .withFollowers(followers)
        .withProducts(products)
        .build();
  }

  @Test
  public void testGetId() {
    assertEquals(1L, seeker.getId());
  }

  @Test
  public void testGetName() {
    assertEquals("Rifqi", seeker.getName());
  }

  @Test
  public void testGetEmail() {
    assertEquals("rifqi@gmail.com", seeker.getEmail());
  }

  @Test
  public void testGetPassword() {
    assertEquals("cobaCoba0808", seeker.getPassword());
  }

  @Test
  public void testGetLocation() {
    assertEquals("Bogor", seeker.getLocation());
  }

  @Test
  public void testGetPhoneNumber() {
    assertEquals("080808080808", seeker.getPhoneNum());
  }

  @Test
  public void testGetChats() {
    assertEquals(chats, seeker.getChats());
  }

  @Test
  public void testGetFollowing() {
    assertEquals(following, seeker.getFollowing());
  }

  @Test
  public void testSetName() {
    seeker.setName("Iqfir");
    assertEquals("Iqfir", seeker.getName());
  }

  @Test
  public void testSetEmail() {
    seeker.setEmail("iqfir@gmail.com");
    assertEquals("iqfir@gmail.com", seeker.getEmail());
  }

  @Test
  public void testSetPassword() {
    seeker.setPassword("0808Cobacoba");
    assertEquals("0808Cobacoba", seeker.getPassword());
  }

  @Test
  public void testSetLocation() {
    seeker.setLocation("Depok");
    assertEquals("Depok", seeker.getLocation());
  }

  @Test
  public void testSetPhoneNumber() {
    seeker.setPhoneNum("081208120812");
    assertEquals("081208120812", seeker.getPhoneNum());
  }

  @Test
  public void testUpdate() {
    assertEquals(0, seeker.getProducts().size());
    seeker.addFollowing(seller);
    seller.addProduct(new Product(1, "piscok", "piscok enak", 1000, seller));
    assertEquals(1, seeker.getProducts().size());
    seeker.update();
    assertEquals(1, seeker.getProducts().size());
  }

  @Test
  public void testAddFollowing() {
    assertEquals(0, seeker.getFollowing().size());
    seeker.addFollowing(seller);
    assertEquals(1, seeker.getFollowing().size());
  }

  @Test
  public void testRemoveFollowing() {
    assertEquals(0, seeker.getFollowing().size());
    seeker.addFollowing(seller);
    assertEquals(1, seeker.getFollowing().size());
    seeker.removeFollowing(seller);
    assertEquals(0, seeker.getFollowing().size());
  }

  @Test
  public void testAddChat() {
    Chat exampleChat = new Chat(seeker, seller);
    seeker.addChat(exampleChat);
    assertEquals(1, seeker.getChats().size());
  }

  @Test
  public void testRemoveChat() {
    Chat exampleChat = new Chat(seeker, seller);
    seeker.addChat(exampleChat);
    assertEquals(1, seeker.getChats().size());
    seeker.removeChat(exampleChat);
    assertEquals(0, seeker.getChats().size());
  }

  @Test
  public void testGetRole() {
    assertEquals("ROLE_SEEKER", seeker.getRole());
  }
}