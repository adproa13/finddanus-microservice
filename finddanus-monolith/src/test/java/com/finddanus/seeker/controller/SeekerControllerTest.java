package com.finddanus.seeker.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import com.finddanus.login.model.FindDanusUserDetails;
import com.finddanus.seeker.model.Seeker;
import com.finddanus.seeker.model.SeekerBuilder;
import com.finddanus.seeker.service.SeekerServiceImpl;
import com.finddanus.seller.model.Seller;
import com.finddanus.seller.model.SellerBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;

@WebMvcTest(controllers = SeekerController.class)
public class SeekerControllerTest {

  @Autowired
  private WebApplicationContext context;

  @MockBean
  private Seeker seeker;

  @MockBean
  private Seller seller;

  @MockBean
  private SeekerServiceImpl seekerService;

  private MockMvc mockMvc;
  private FindDanusUserDetails userDetails;
  private MockHttpSession session;
  private MockMvc mockMvc2;
  private FindDanusUserDetails userDetails2;
  private MockHttpSession session2;

  @BeforeEach
  public void setUp() {
    seeker = SeekerBuilder.createSeeker()
        .withId(1)
        .withName("Rifqi")
        .withEmail("rifqi@gmail.com")
        .withPassword("12345678")
        .withLocation("Bogor")
        .withPhoneNum("082208220822")
        .withChats(new ArrayList<>())
        .withFollowing(new ArrayList<>())
        .build();
    userDetails =  new FindDanusUserDetails(seeker);
    mockMvc = MockMvcBuilders
        .webAppContextSetup(context)
        .apply(SecurityMockMvcConfigurers.springSecurity())
        .build();
    session = new MockHttpSession();
    session.setAttribute("id", (long)1);
    seller = SellerBuilder.createSeller()
        .withId(2)
        .withName("Michael")
        .withEmail("michael@gmail.com")
        .withPassword("12345678")
        .withLocation("Jakarta")
        .withPhoneNum("082208220820")
        .withChats(new ArrayList<>())
        .withFollowers(new ArrayList<>())
        .withProducts(new ArrayList<>())
        .build();
    userDetails2 =  new FindDanusUserDetails(seller);
    mockMvc2 = MockMvcBuilders
        .webAppContextSetup(context)
        .apply(SecurityMockMvcConfigurers.springSecurity())
        .build();
    session2 = new MockHttpSession();
    session2.setAttribute("id", (long)2);
  }

  @Test
  public void testGetSeekerProfileFound() throws Exception {
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/profile")
        .with(user(userDetails))
        .session(session);
    mockMvc.perform(requestBuilder)
        .andExpect(status().isOk())
        .andExpect(view().name("seeker/seekerProfile"));
  }

  @Test
  public void testGetSeekerHomepageNotFound() throws Exception {
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/home")
        .with(user(userDetails))
        .session(session);
    mockMvc.perform(requestBuilder)
        .andExpect(status().isOk())
        .andExpect(view().name("seeker/seekerHomepage"));
  }

  @Test
  public void testGetSeekerFollowingPageNotFound() throws Exception {
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/following")
        .with(user(userDetails))
        .session(session);
    mockMvc.perform(requestBuilder)
        .andExpect(status().isOk())
        .andExpect(view().name("seeker/seekerFollowing"));
  }

  @Test
  public void testGetSeekerEditProfileNotFound() throws Exception {
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/edit")
        .with(user(userDetails))
        .session(session);
    mockMvc.perform(requestBuilder)
        .andExpect(status().is3xxRedirection())
        .andExpect(view().name("redirect:/profile"));
  }

  @Test
  public void testShowProductByNameNotFound() throws Exception {
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/search/productName=risol")
        .with(user(userDetails))
        .session(session);
    mockMvc.perform(requestBuilder)
        .andExpect(status().isOk())
        .andExpect(view().name("seeker/seekerSearchResult"));
  }

  @Test
  public void testShowProductByLocationNotFound() throws Exception {
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/search/location=jakarta")
        .with(user(userDetails))
        .session(session);
    mockMvc.perform(requestBuilder)
        .andExpect(status().isOk())
        .andExpect(view().name("seeker/seekerSearchResult"));
  }

  @Test
  public void testSeekerFollowSeller() throws Exception {
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/follow/1")
        .with(user(userDetails))
        .session(session);
    mockMvc.perform(requestBuilder)
        .andExpect(status().is3xxRedirection());
  }

  @Test
  public void testSeekerUnfollowSeller() throws Exception {
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/unfollow/1")
        .with(user(userDetails))
        .session(session);
    mockMvc.perform(requestBuilder)
        .andExpect(status().is3xxRedirection());
  }

  @Test
  public void testSeekerSearchProductNotFound() throws Exception {
    MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    params.add("productName", "risol");
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/home")
        .with(user(userDetails))
        .session(session);
    mockMvc.perform(requestBuilder.params(params))
        .andExpect(handler().methodName("searchProduct"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/search/productName=risol"));
  }

  @Test
  public void testEditSeekerProfileNotFound() throws Exception {
    MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    params.add("seeker_id", "1");
    params.add("name", "test");
    params.add("email", "test@gmail.com");
    params.add("location", "Bogor");
    params.add("phoneNum", "080808080808");
    params.add("passwordConfirm", "seeker1");
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/edit")
        .with(user(userDetails))
        .session(session);
    mockMvc.perform(requestBuilder.params(params))
        .andExpect(handler().methodName("updateProfile"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/edit"));
  }

  @Test
  public void testEditSeekerPasswordNotFound() throws Exception {
    MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    params.add("seeker_id", "1");
    params.add("oldPassword", "seller1");
    params.add("newPassword", "seller01");
    params.add("confirmNewPassword", "seller01");
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/editPass")
        .with(user(userDetails))
        .session(session);
    mockMvc.perform(requestBuilder.params(params))
        .andExpect(handler().methodName("updatePassword"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/edit"));
  }

  @Test
  public void testGetSeekerChatListPageNotFound() throws Exception {
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/chats")
        .with(user(userDetails))
        .session(session);
    mockMvc.perform(requestBuilder)
        .andExpect(status().isOk())
        .andExpect(view().name("seeker/seekerChatList"));
  }

  @Test
  public void testGetSeekerProfilePreviewPage() throws Exception {
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/preview/1")
        .with(user(userDetails2))
        .session(session2);
    mockMvc2.perform(requestBuilder)
        .andExpect(status().isOk())
        .andExpect(view().name("seeker/seekerProfilePreview"));
  }
}
