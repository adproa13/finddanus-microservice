//package com.finddanus.seeker.service;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//import static org.junit.jupiter.api.Assertions.assertNull;
//
//import com.finddanus.FindDanusApplication;
//import com.finddanus.chat.model.Chat;
//import com.finddanus.seeker.model.Observer;
//import com.finddanus.seeker.model.Seeker;
//import com.finddanus.seeker.model.SeekerBuilder;
//import com.finddanus.seeker.repository.SeekerRepository;
//import com.finddanus.seller.model.Product;
//import com.finddanus.seller.model.Seller;
//import com.finddanus.seller.model.SellerBuilder;
//import com.finddanus.seller.model.Subject;
//import com.finddanus.seller.repository.ProductRepository;
//import com.finddanus.seller.repository.SellerRepository;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Optional;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.test.annotation.DirtiesContext;
//
//@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
//@SpringBootTest(classes = FindDanusApplication.class)
//public class SeekerServiceImplTest {
//  private List<Chat> chats;
//  private List<Subject> following;
//  private List<Observer> followers;
//  private List<Product> products;
//  private Seeker seeker;
//  private Seeker seeker2;
//  private Seller seller;
//  private Seller seller2;
//  private Product product1;
//  private Product product2;
//
//  @Autowired
//  private SeekerRepository seekerRepository;
//
//  @Autowired
//  private SellerRepository sellerRepository;
//
//  @Autowired
//  private ProductRepository productRepository;
//
//  @Autowired
//  private SeekerServiceImpl seekerServiceImpl;
//
//  @Autowired
//  private PasswordEncoder passwordEncoder;
//
//  /**
//   * set up test.
//   */
//  @BeforeEach
//  public void setUp() {
//    chats = new ArrayList<>();
//    following = new ArrayList<>();
//    followers = new ArrayList<>();
//    products = new ArrayList<>();
//    seeker = SeekerBuilder.createSeeker()
//        .withId(1)
//        .withName("Rifqi")
//        .withEmail("rifqi@gmail.com")
//        .withPassword(passwordEncoder.encode("cobaCoba0808"))
//        .withLocation("Bogor")
//        .withPhoneNum("080808080808")
//        .withChats(chats)
//        .withFollowing(following)
//        .build();
//    seekerRepository.save(seeker);
//    seeker2 = SeekerBuilder.createSeeker()
//        .withId(2)
//        .withName("Rifqo")
//        .withEmail("rifqo@gmail.com")
//        .withPassword(passwordEncoder.encode("cobaCoba0909"))
//        .withLocation("Bogor")
//        .withPhoneNum("080808080809")
//        .withChats(chats)
//        .withFollowing(following)
//        .build();
//    seller = SellerBuilder.createSeller()
//        .withId(1)
//        .withName("Michael")
//        .withEmail("michael@gmail.com")
//        .withPassword(passwordEncoder.encode("12345678"))
//        .withLocation("Jakarta")
//        .withPhoneNum("081234567890")
//        .withChats(chats)
//        .withFollowers(followers)
//        .withProducts(products)
//        .build();
//    sellerRepository.save(seller);
//    seller2 = SellerBuilder.createSeller()
//        .withId(2)
//        .withName("Michaela")
//        .withEmail("michaela@gmail.com")
//        .withPassword(passwordEncoder.encode("12345678"))
//        .withLocation("Bekasi")
//        .withPhoneNum("081234567891")
//        .withChats(chats)
//        .withFollowers(followers)
//        .withProducts(products)
//        .build();
//    product1 = new Product(1, "risol", "risol enak", 1000, seller);
//    product2 = new Product(2, "risol", "risol mayo", 2000, seller2);
//  }
//
//  @Test
//  public void testAddSeeker() {
//    int jumlahSeeker = seekerRepository.findAll().size();
//    seekerServiceImpl.addSeeker(seeker2);
//    assertEquals(jumlahSeeker + 1, seekerRepository.findAll().size());
//  }
//
//  @Test
//  public void testFindSeeker() {
//    Optional<Seeker> targetSeeker = seekerServiceImpl.findSeeker(1);
//    assertEquals("Rifqi", targetSeeker.get().getName());
//  }
//
//  @Test
//  public void testFindSeller() {
//    sellerRepository.save(seller2);
//    Optional<Seller> targetSeller = seekerServiceImpl.findSeller(2);
//    assertEquals("Michaela", targetSeller.get().getName());
//  }
//
//  @Test
//  public void testGetSeekerObjectByIdIfExist() {
//    Seeker resultSeeker = seekerServiceImpl.getSeekerObjectById(1);
//    assertEquals("Rifqi", resultSeeker.getName());
//  }
//
//  @Test
//  public void testGetSeekerObjectByIdIfNotExist() {
//    Seeker resultSeeker = seekerServiceImpl.getSeekerObjectById(4);
//    assertNull(resultSeeker);
//  }
//
//  @Test
//  public void testgetSellerObjectByIdIfExist() {
//    sellerRepository.save(seller2);
//    Seller resultSeller = seekerServiceImpl.getSellerObjectById(2);
//    assertEquals("Michaela", resultSeller.getName());
//  }
//
//  @Test
//  public void testgetSellerObjectByIdIfNotExist() {
//    Seller resultSeller = seekerServiceImpl.getSellerObjectById(20);
//    assertNull(resultSeller);
//  }
//
//  @Test
//  public void testFollowSeller() {
//    seekerServiceImpl.followSeller(2, 2);
//  }
//
//  @Test
//  public void testUnfollowSeller() {
//    seekerServiceImpl.unfollowSeller(1, 1);
//    assertEquals(0, seekerServiceImpl.showFollowing(1).size());
//  }
//
//  @Test
//  public void testShowFollowing() {
//    List<Subject> following = seekerServiceImpl.showFollowing(1);
//    assertEquals(0, following.size());
//  }
//
//  @Test
//  public void testEditSeekerProfile() {
//    Seeker seeker = seekerServiceImpl.editSeekerProfile(
//        1,"Rifqi", "rifqi@gmail.com",
//        "Bogor","080808080808", "cobaCoba0808");
//    assertNotNull(seeker);
//  }
//
//  @Test
//  public void testEditSeekerProfileIfNull() {
//    Seeker seeker = seekerServiceImpl.editSeekerProfile(
//        20,"Rifqi", "rifqi@gmail.com",
//        "Bogor","080808080808", "cobaCoba0808");
//    assertNull(seeker);
//  }
//
//  @Test
//  public void testEditSeekerPassword() {
//    Seeker seeker = seekerServiceImpl.editSeekerPassword(
//        1,"cobaCoba0808",
//        "cobaCoba08", "cobaCoba08");
//    assertNotNull(seeker);
//  }
//
//  @Test
//  public void testEditSeekerPasswordIfNull() {
//    Seeker seeker = seekerServiceImpl.editSeekerPassword(
//        20,"cobaCoba0808",
//        "cobaCoba08", "cobaCoba08");
//    assertNull(seeker);
//  }
//
//  @Test
//  public void testShowProductByLocation() {
//    List<Product> productsInJakarta = seekerServiceImpl.showProductByLocation("Jakarta");
//    assertEquals(0, productsInJakarta.size());
//  }
//
//  @Test
//  public void testShowProductByName() {
//    productRepository.save(product2);
//    List<Product> productsNamedRisol = seekerServiceImpl.showProductByName("risol");
//    assertEquals(1, productsNamedRisol.size());
//  }
//
//  @Test
//  public void testShowHomepage() {
//    List<Product> products = seekerServiceImpl.showHomepage(1);
//    assertNotNull(products);
//  }
//
//  @Test
//  public void testGetChatsIfExists() {
//    List<Chat> chats = seekerServiceImpl.getChats(1);
//    assertNotNull(chats);
//  }
//
//  @Test
//  public void testGetChatsIfNotExists() {
//    List<Chat> chats = seekerServiceImpl.getChats(10);
//    assertNull(chats);
//  }
//}
//
