package com.finddanus.seller.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import com.finddanus.login.model.FindDanusUserDetails;
import com.finddanus.seeker.model.Seeker;
import com.finddanus.seeker.model.SeekerBuilder;
import com.finddanus.seller.model.Product;
import com.finddanus.seller.model.Seller;
import com.finddanus.seller.model.SellerBuilder;
import com.finddanus.seller.service.SellerServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;

@WebMvcTest(controllers = SellerController.class)
public class SellerControllerTest {

  @Autowired
  private WebApplicationContext context;

  @MockBean
  private Seller seller;

  @MockBean
  private Product product;

  @MockBean
  private SellerServiceImpl sellerService;

  private MockMvc mockMvc;
  private FindDanusUserDetails userDetails;
  private MockHttpSession session;

  @BeforeEach
  public void setUp() {
    seller = SellerBuilder.createSeller()
        .withId(1)
        .withName("Michael")
        .withEmail("michael@gmail.com")
        .withPassword("12345678")
        .withLocation("Jakarta")
        .withPhoneNum("081234567890")
        .withChats(new ArrayList<>())
        .withFollowers(new ArrayList<>())
        .withProducts(new ArrayList<>())
        .build();
    userDetails = new FindDanusUserDetails(seller);
    mockMvc = MockMvcBuilders
        .webAppContextSetup(context)
        .apply(springSecurity())
        .build();
    session = new MockHttpSession();
    session.setAttribute("id", (long)1);
  }

  @Test
  public void testGetAllSellers() throws Exception {
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/seller")
        .with(user(userDetails))
        .session(session);
    mockMvc.perform(requestBuilder)
        .andExpect(status().isOk())
        .andExpect(model().attributeExists("sellers"))
        .andExpect(view().name("seller"));
  }

  @Test
  public void testGetSellerProfileNotFound() throws Exception {
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/seller/home")
        .with(user(userDetails))
        .session(session);
    mockMvc.perform(requestBuilder)
        .andExpect(status().isOk())
        .andExpect(view().name("seller/sellerInfo"));
  }

  @Test
  public void testGetSellerEditProfileNotFound() throws Exception {
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/seller/edit")
        .with(user(userDetails))
        .session(session);
    mockMvc.perform(requestBuilder)
        .andExpect(status().is3xxRedirection())
        .andExpect(view().name("redirect:/seller/home"));
  }

  @Test
  public void testEditSellerProfileNotFound() throws Exception {
    MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    params.add("seller_id", "1");
    params.add("name", "test");
    params.add("email", "test@gmail.com");
    params.add("location", "Jakarta");
    params.add("phoneNum", "081212121212");
    params.add("passwordConfirm", "seller1");

    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/seller/edit")
        .with(user(userDetails))
        .session(session);
    mockMvc.perform(requestBuilder.params(params))
        .andExpect(handler().methodName("updateSellerProfile"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/seller/edit"));
  }

  @Test
  public void testEditSellerPasswordNotFound() throws Exception {
    MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    params.add("seller_id", "1");
    params.add("oldPassword", "asd");
    params.add("password", "seller1");
    params.add("passwordConfirmChange", "seller1");

    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/seller/editPass")
        .with(user(userDetails))
        .session(session);
    mockMvc.perform(requestBuilder.params(params))
        .andExpect(handler().methodName("updateSellerPassword"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/seller/edit"));
  }

  @Test
  public void testGetFollowers() throws Exception {
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/seller/followers")
        .with(user(userDetails))
        .session(session);
    mockMvc.perform(requestBuilder)
        .andExpect(status().isOk())
        .andExpect(view().name("seller/sellerFollowers"));
  }

  @Test
  public void testAddProductFail() throws Exception {
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/seller/addProduct")
        .with(user(userDetails))
        .session(session);
    mockMvc.perform(requestBuilder)
        .andExpect(status().is3xxRedirection())
        .andExpect(view().name("redirect:/seller/home"));
  }

  @Test
  public void testAddProductWithSellerId() throws Exception {
    MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    params.add("seller_id", "1");
    params.add("title", "productTitle");
    params.add("desc", "productDescription");
    params.add("price", "1234");

    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/seller/addProduct")
        .with(user(userDetails))
        .session(session);
    mockMvc.perform(requestBuilder.params(params))
        .andExpect(handler().methodName("addProduct"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/seller/home"));
  }

  @Test
  public void testRemoveProduct() throws Exception {
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/seller/removeProduct/1")
        .with(user(userDetails))
        .session(session);
    mockMvc.perform(requestBuilder)
        .andExpect(status().is3xxRedirection())
        .andExpect(view().name("redirect:/seller/home"));
  }

  @Test
  public void testGetChats() throws Exception {
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/seller/chats")
        .with(user(userDetails))
        .session(session);
    mockMvc.perform(requestBuilder)
        .andExpect(status().isOk())
        .andExpect(view().name("seller/sellerChatList"));
  }

  @Test
  public void testPreviewSellerProfile() throws Exception {
    Seeker seeker = SeekerBuilder.createSeeker()
        .withId(99)
        .withName("Michael")
        .withEmail("michael@gmail.com")
        .withPassword("12345678")
        .withLocation("Jakarta")
        .withPhoneNum("081234567890")
        .withChats(new ArrayList<>())
        .withFollowing(new ArrayList<>())
        .build();
    FindDanusUserDetails seekerDetails = new FindDanusUserDetails(seeker);
    MockHttpSession seekerSession = new MockHttpSession();
    seekerSession.setAttribute("id", (long)99);
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/seller/preview/1")
        .with(user(seekerDetails))
        .session(seekerSession);
    mockMvc.perform(requestBuilder)
        .andExpect(status().isOk())
        .andExpect(view().name("seller/sellerInfoPreview"));
  }
}
