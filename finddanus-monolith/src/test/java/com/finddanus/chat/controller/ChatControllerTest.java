//package com.finddanus.chat.controller;
//
//import com.finddanus.chat.model.Chat;
//import com.finddanus.login.model.FindDanusUserDetails;
//import com.finddanus.seeker.model.Seeker;
//import com.finddanus.seeker.model.SeekerBuilder;
//import com.finddanus.seller.model.Seller;
//import com.finddanus.seller.model.SellerBuilder;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.mock.web.MockHttpSession;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.util.LinkedMultiValueMap;
//import org.springframework.util.MultiValueMap;
//import org.springframework.web.context.WebApplicationContext;
//
//import java.util.ArrayList;
//
//import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
//import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//public class ChatControllerTest {
//
//  @Autowired
//  private WebApplicationContext context;
//
//  @MockBean
//  private Chat chat;
//
//  @MockBean
//  private Seeker seeker;
//
//  @MockBean
//  private Seller seller;
//
//  private MockMvc mockMvc;
//  private FindDanusUserDetails userDetailsSeeker;
//  private FindDanusUserDetails userDetailsSeller;
//  private MockHttpSession session;
//
//  @BeforeEach
//  public void setUp(){
//    seller = SellerBuilder.createSeller()
//        .withId(2)
//        .withName("Michael")
//        .withEmail("michael@gmail.com")
//        .withPassword("12345678")
//        .withLocation("Jakarta")
//        .withPhoneNum("081234567890")
//        .withChats(new ArrayList<>())
//        .withFollowers(new ArrayList<>())
//        .withProducts(new ArrayList<>())
//        .build();
//    seeker = SeekerBuilder.createSeeker()
//        .withId(3)
//        .withName("Rifqi")
//        .withEmail("rifqi@gmail.com")
//        .withPassword("cobaCoba0808")
//        .withLocation("Bogor")
//        .withPhoneNum("080808080808")
//        .withChats(new ArrayList<>())
//        .withFollowing(new ArrayList<>())
//        .build();
//    chat = new Chat(seeker, seller);
//    chat.setId(1);
//    userDetailsSeeker = new FindDanusUserDetails(seeker);
//    userDetailsSeller = new FindDanusUserDetails(seller);
//    mockMvc = MockMvcBuilders
//        .webAppContextSetup(context)
//        .apply(springSecurity())
//        .build();
//  }
//
//  @Test
//  public void testGetSeekerChat() throws Exception{
//    try {
//      session = new MockHttpSession();
//      session.setAttribute("id", (long) 3);
//      MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/chat/1")
//          .with(user(userDetailsSeeker))
//          .session(session);
//      mockMvc.perform(requestBuilder)
//          .andExpect(status().isOk())
//          .andExpect(model().attributeExists("chat"))
//          .andExpect(view().name("chatSeeker"));
//    }catch(Exception e){}
//  }
//
//  @Test
//  public void testGetSellerChat() throws Exception{
//    try {
//      session = new MockHttpSession();
//      session.setAttribute("id", (long) 2);
//      MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/seller/chat/1")
//          .with(user(userDetailsSeller))
//          .session(session);
//      mockMvc.perform(requestBuilder)
//          .andExpect(status().isOk())
//          .andExpect(model().attributeExists("chat"))
//          .andExpect(view().name("chatSeller"));
//    }catch(Exception e){}
//  }
//
//  @Test
//  public void testSeekerSendMessage() throws Exception{
//    try {
//      session = new MockHttpSession();
//      session.setAttribute("id", (long) 4);
//      MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
//      params.add("seeker_id", "3");
//      params.add("chat_id", "1");
//      params.add("message", "halo");
//      MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/chat/send")
//          .with(user(userDetailsSeeker))
//          .session(session);
//      mockMvc.perform(requestBuilder.params(params))
//          .andExpect(status().is3xxRedirection())
//          .andExpect(handler().methodName("seekerSendMessage"));
//    }catch(Exception e){}
//  }
//
//  @Test
//  public void testSellerSendMessage() throws Exception{
//    try {
//      session = new MockHttpSession();
//      session.setAttribute("id", (long) 5);
//      MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
//      params.add("seller_id", "3");
//      params.add("chat_id", "1");
//      params.add("message", "halo");
//      MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/seller/chat/send")
//          .with(user(userDetailsSeller))
//          .session(session);
//      mockMvc.perform(requestBuilder.params(params))
//          .andExpect(status().is3xxRedirection())
//          .andExpect(handler().methodName("sellerSendMessage"));
//    }catch(Exception e){}
//  }
//}
