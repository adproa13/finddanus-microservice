package com.finddanus.chat.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.finddanus.domain.model.User;
import com.finddanus.seeker.model.Seeker;
import com.finddanus.seeker.model.SeekerBuilder;
import com.finddanus.seller.model.Product;
import com.finddanus.seller.model.Seller;
import com.finddanus.seller.model.SellerBuilder;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class ChatTest {
  private Seeker seeker;
  private Seller seller;
  private List<Chat> chats;
  private List<Seller> following;
  private List<Seeker> followers;
  private List<Product> products;
  private Chat chat;

  /**
   * set up test.
   */
  @BeforeEach
  public void setUp() {
    seeker = SeekerBuilder.createSeeker()
        .withId(1)
        .withName("Rifqi")
        .withEmail("rifqi@gmail.com")
        .withPassword("cobaCoba0808")
        .withLocation("Bogor")
        .withPhoneNum("080808080808")
        .withChats(chats)
        .withFollowing(new ArrayList<>())
        .build();

    seller = SellerBuilder.createSeller()
        .withId(1)
        .withName("Michael")
        .withEmail("michael@gmail.com")
        .withPassword("12345678")
        .withLocation("Jakarta")
        .withPhoneNum("081234567890")
        .withChats(chats)
        .withFollowers(new ArrayList<>())
        .withProducts(products)
        .build();
    chat = new Chat(seeker, seller);
    chat.setId(1);
  }

  @Test
  public void testGetSeeker() {
    assertEquals(seeker.getName(), chat.getSeeker().getName());
  }

  @Test
  public void testGetSeller() {
    assertEquals(seller.getName(), chat.getSeller().getName());
  }

  @Test
  public void testAddMessageOwner() {
    chat.addMessage(seeker, "hi");
    chat.addMessage(seller, "hello");
    List<Message> ml = chat.getMessageList();
    Message m = ml.get(0);
    Message m2 = ml.get(1);
    Seeker targetSeeker = m.getSeeker();
    Seller targetSeller = m2.getSeller();
    assertEquals(seeker, targetSeeker);
    assertEquals(seller, targetSeller);

  }

  @Test
  public void testAddMessageContent() {
    chat.addMessage(seeker, "hi");
    chat.addMessage(seller, "hello");
    List<Message> ml = chat.getMessageList();
    Message m = ml.get(0);
    Message m2 = ml.get(1);
    String s = m.getContent();
    String s2 = m2.getContent();
    assertEquals("hi", s);
    assertEquals("hello", s2);
  }
  @Test
  public void testChatId(){
    assertEquals(1, chat.getId());
  }

}
