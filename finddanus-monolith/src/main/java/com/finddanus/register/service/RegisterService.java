package com.finddanus.register.service;

import com.finddanus.seeker.model.Seeker;
import com.finddanus.seller.model.Seller;

public interface RegisterService {
  public Seeker createSeeker(String name, String email, String location,
                             String phoneNum, String password, String passwordConfirm);

  public Seller createSeller(String name, String email, String location,
                             String phoneNum, String password, String passwordConfirm);
}

