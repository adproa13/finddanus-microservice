package com.finddanus.register.service;

import com.finddanus.seeker.model.Seeker;
import com.finddanus.seeker.model.SeekerBuilder;
import com.finddanus.seeker.repository.SeekerRepository;
import com.finddanus.seller.model.Seller;
import com.finddanus.seller.model.SellerBuilder;
import com.finddanus.seller.repository.SellerRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class RegisterServiceImpl implements RegisterService {

  @Autowired
  private final SeekerRepository seekerRepository;

  @Autowired
  private final SellerRepository sellerRepository;

  @Autowired
  private final PasswordEncoder passwordEncoder;

  /**
   * Service that serves register feature.
   * @param seekerRepository of seeker
   * @param sellerRepository of seller
   * @param passwordEncoder for password
   */
  public RegisterServiceImpl(SeekerRepository seekerRepository,
                             SellerRepository sellerRepository,
                             PasswordEncoder passwordEncoder) {
    this.seekerRepository = seekerRepository;
    this.sellerRepository = sellerRepository;
    this.passwordEncoder = passwordEncoder;
  }

  @Override
  public Seeker createSeeker(String name, String email, String location,
                             String phoneNum, String password, String passwordConfirm) {
    if (password.equals(passwordConfirm)) {
      Seeker seeker = SeekerBuilder.createSeeker()
          .withName(name)
          .withEmail(email)
          .withLocation(location)
          .withPhoneNum(phoneNum)
          .withPassword(passwordEncoder.encode(password))
          .withChats(new ArrayList<>())
          .withFollowing(new ArrayList<>())
          .build();
      return seekerRepository.save(seeker);
    }
    return null;
  }

  @Override
  public Seller createSeller(String name, String email, String location,
                             String phoneNum, String password, String passwordConfirm) {
    if (password.equals(passwordConfirm)) {
      Seller seller = SellerBuilder.createSeller()
          .withName(name)
          .withEmail(email)
          .withLocation(location)
          .withPhoneNum(phoneNum)
          .withPassword(passwordEncoder.encode(password))
          .withChats(new ArrayList<>())
          .withFollowers(new ArrayList<>())
          .withProducts(new ArrayList<>())
          .build();
      return sellerRepository.save(seller);
    }
    return null;
  }
}
