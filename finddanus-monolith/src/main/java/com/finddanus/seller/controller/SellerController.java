package com.finddanus.seller.controller;

import com.finddanus.chat.model.Chat;
import com.finddanus.login.model.FindDanusUserDetails;
import com.finddanus.seller.model.Product;
import com.finddanus.seller.model.Seller;
import com.finddanus.seller.service.SellerService;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(path = "/seller")
public class SellerController {

  @Autowired
  private final SellerService sellerService;

  public SellerController(SellerService sellerService) {
    this.sellerService = sellerService;
  }

  @GetMapping
  public String seller(Model model) {
    model.addAttribute("sellers", sellerService.findAllSellers());
    return "seller";
  }

  /**
   * Returns profile of authenticated seller.
   * @param model for the view
   * @param request request data
   * @param auth current authenticated user
   * @return seller/sellerInfo.html
   */
  @GetMapping("/home")
  public String getProductsBySellerId(
      Model model,
      HttpServletRequest request,
      Authentication auth
  ) {
    long id = ((FindDanusUserDetails) auth.getPrincipal()).getId();
    request.getSession().setAttribute("id", id);
    Optional<Seller> seller = sellerService.findSeller(id);
    List<Product> products = sellerService.findProductsBySellerId(id);
    try {
      model.addAttribute("products", products);
      model.addAttribute("seekers", sellerService.getFollowers(id).size());
      model.addAttribute("seller", seller.get());
    } catch (Exception e) {
      model.addAttribute("notFound", true);
    }
    return "seller/sellerInfo";
  }

  /**
   * Returns edit profile page for seller.
   * @param model for view
   * @param request request data
   * @return seller/sellerEdit if seller exists else redirect to profile page
   */
  @GetMapping("/edit")
  public String updateSellerProfilePage(Model model, HttpServletRequest request) {
    long id = (long) request.getSession().getAttribute("id");
    try {
      model.addAttribute("seller", sellerService.findSeller(id).get());
      return "seller/sellerEdit";
    } catch (NoSuchElementException e) {
      return "redirect:/seller/home";
    }
  }

  /**
   * Send data for edit profile.
   * @param sellerId of the seller
   * @param name of seller
   * @param email of seller
   * @param location of seller
   * @param phoneNum of seller
   * @param passwordConfirm by requesting seller
   * @return redirect to profile page if success else edit page
   */
  @PostMapping("/edit")
  public String updateSellerProfile(
      @RequestParam(value = "seller_id") long sellerId,
      @RequestParam(value = "name") String name,
      @RequestParam(value = "email") String email,
      @RequestParam(value = "location") String location,
      @RequestParam(value = "phoneNum") String phoneNum,
      @RequestParam(value = "passwordConfirm") String passwordConfirm
  ) {
    if (sellerService.editProfile(sellerId,
        name,
        email,
        location,
        phoneNum,
        passwordConfirm) != null) {
      return "redirect:/seller/home";
    }
    return "redirect:/seller/edit";
  }

  /**
   * Send data for change password.
   * @param sellerId of the seller
   * @param oldPassword of seller
   * @param password of seller
   * @param passwordConfirm by seller
   * @return redirect to profile page if success else edit page
   */
  @PostMapping("/editPass")
  public String updateSellerPassword(
      @RequestParam(value = "seller_id") long sellerId,
      @RequestParam(value = "oldPassword") String oldPassword,
      @RequestParam(value = "password") String password,
      @RequestParam(value = "passwordConfirmChange") String passwordConfirm
  ) {
    if (sellerService.editPassword(sellerId, oldPassword, password, passwordConfirm) != null) {
      return "redirect:/seller/home";
    }
    return "redirect:/seller/edit";
  }

  /**
   * Shows seller's followers.
   * @param model for view
   * @param request request data
   * @return seller/sellerFollowers.html page
   */
  @GetMapping("/followers")
  public String getFollowers(Model model, HttpServletRequest request) {
    long id = (long) request.getSession().getAttribute("id");
    try {
      model.addAttribute("seekers", sellerService.getFollowers(id));
      model.addAttribute("seller", sellerService.findSeller(id).get());
    } catch (NoSuchElementException e) {
      model.addAttribute("noFollowers", true);
    }
    return "seller/sellerFollowers";
  }

  /**
   * Get product page.
   * @param model for view
   * @param request request data
   * @return addProduct.html if success else redirect to profile
   */
  @GetMapping("/addProduct")
  public String getAddProductPage(Model model, HttpServletRequest request) {
    long id = (long) request.getSession().getAttribute("id");
    try {
      model.addAttribute("seller", sellerService.findSeller(id).get());
      return "seller/addProduct";
    } catch (NoSuchElementException e) {
      return "redirect:/seller/home";
    }
  }

  /**
   * Add product to database.
   * @param sellerId of product
   * @param title of product
   * @param desc of product
   * @param price of product
   * @return redirect to seller profile
   */
  @PostMapping("/addProduct")
  public String addProduct(
      @RequestParam(value = "seller_id") long sellerId,
      @RequestParam(value = "title") String title,
      @RequestParam(value = "desc") String desc,
      @RequestParam(value = "price") int price
  ) {
    Optional<Seller> seller = sellerService.findSeller(sellerId);
    try {
      sellerService.addProduct(new Product(title, desc, price, seller.get()));
    } catch (NoSuchElementException e) {
      return "redirect:/seller/home";
    }
    return "redirect:/seller/home";
  }

  /**
   * Remove specified product from database.
   * @param pid of product
   * @return redirect to seller's homepage
   */
  @GetMapping("/removeProduct/{pid}")
  public String removeProduct(
      @PathVariable long pid
  ) {
    sellerService.removeProduct(pid);
    return "redirect:/seller/home";
  }

  /**
   * Show chatlists for currently authenticated seller.
   * @param model for view
   * @param request request data
   * @return seller/sellerChatList.html
   */
  @GetMapping("/chats")
  public String getChats(Model model, HttpServletRequest request) {
    long id = (long) request.getSession().getAttribute("id");
    List<Chat> chats = sellerService.getChats(id);
    model.addAttribute("chats", chats);
    return "seller/sellerChatList";
  }

  /**
   * Show seller's profile from seeker's view.
   * @param id of seller to be previewed
   * @param model for view
   * @return preview mode of seller profile
   */
  @GetMapping("/preview/{id}")
  public String previewSellerProfile(@PathVariable long id, Model model) {
    Optional<Seller> seller = sellerService.findSeller(id);
    List<Product> products = sellerService.findProductsBySellerId(id);
    try {
      model.addAttribute("products", products);
      model.addAttribute("seekers", sellerService.getFollowers(id).size());
      model.addAttribute("seller", seller.get());
    } catch (Exception e) {
      model.addAttribute("notFound", true);
    }
    return "seller/sellerInfoPreview";
  }

}
