package com.finddanus.seller.model;

import com.finddanus.seeker.model.Observer;
import java.util.List;

public interface Subject {
  public void addFollower(Observer observer);

  public void removeFollower(Observer observer);

  public void notifyFollowers();

  public List<Product> getProducts();

  public void addProduct(Product product);

  public void removeProduct(Product product);

  public long getId();
}
