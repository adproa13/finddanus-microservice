package com.finddanus.seller.model;

import com.finddanus.chat.model.Chat;
import com.finddanus.domain.model.User;
import com.finddanus.seeker.model.Observer;
import com.finddanus.seeker.model.Seeker;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "seller")
public class Seller extends User implements Subject {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "seller_id", nullable = false, updatable = false)
  private long id;

  @Column(name = "role", columnDefinition = "varchar(255) default 'ROLE_SELLER'")
  private String role;

  @OneToMany(
      mappedBy = "seller",
      cascade = CascadeType.ALL,
      fetch = FetchType.LAZY
  )
  private List<Chat> chats;

  @ManyToMany(
      fetch = FetchType.EAGER,
      cascade = CascadeType.ALL,
      targetEntity = Seeker.class
  )
  @JoinTable(
      name = "Seeker_Seller",
      joinColumns = @JoinColumn(name = "seller_id"),
      inverseJoinColumns = @JoinColumn(name = "seeker_id")
  )
  private List<Observer> followers;

  @OneToMany(
      mappedBy = "seller",
      fetch = FetchType.LAZY,
      cascade = CascadeType.ALL
  )
  private List<Product> products;

  /**
   * Create user who will sell products.
   * @param builder accepts builder object from SellerBuilder
   */
  public Seller(SellerBuilder builder) {
    this.id = builder.getId();
    this.name = builder.getName();
    this.email = builder.getEmail();
    this.password = builder.getPassword();
    this.location = builder.getLocation();
    this.phoneNum = builder.getPhoneNum();
    this.chats = builder.getChats();
    this.followers = builder.getFollowers();
    this.products = builder.getProducts();
    role = "ROLE_SELLER";
  }

  public Seller() {}

  public List<Observer> getFollowers() {
    return followers;
  }

  @Override
  public List<Product> getProducts() {
    return products;
  }

  @Override
  public void addFollower(Observer follower) {
    followers.add(follower);
    follower.update();
  }

  @Override
  public void removeFollower(Observer follower) {
    followers.remove(follower);
    follower.update();
  }

  @Override
  public void addProduct(Product product) {
    products.add(product);
    notifyFollowers();
  }

  @Override
  public void removeProduct(Product product) {
    products.remove(product);
    notifyFollowers();
  }

  @Override
  public void notifyFollowers() {
    for (Observer follower : followers) {
      follower.update();
    }
  }

  public void addChat(Chat chat) {
    chats.add(chat);
  }

  public void removeChat(Chat chat) {
    chats.remove(chat);
  }

  public List<Chat> getChats() {
    return chats;
  }

  @Override
  public long getId() {
    return id;
  }

  @Override
  public String getRole() {
    return role;
  }
}
