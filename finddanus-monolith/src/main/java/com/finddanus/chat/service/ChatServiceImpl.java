package com.finddanus.chat.service;

import com.finddanus.chat.model.Chat;
import com.finddanus.chat.repository.ChatRepository;
import com.finddanus.chat.repository.MessageRepository;
import com.finddanus.seeker.model.Seeker;
import com.finddanus.seeker.service.SeekerService;
import com.finddanus.seller.model.Seller;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChatServiceImpl implements ChatService {

  @Autowired
  private ChatRepository chatRepository;

  @Autowired
  private MessageRepository messageRepository;

  @Autowired
  private SeekerService seekerService;

  /**
   * Service that serves chat.
   * @param chatRepository of chat
   * @param messageRepository of message
   * @param seekerService for seeker and seller
   */
  public ChatServiceImpl(ChatRepository chatRepository,
                         MessageRepository messageRepository,
                         SeekerService seekerService) {
    this.chatRepository = chatRepository;
    this.messageRepository = messageRepository;
    this.seekerService = seekerService;
  }

  @Override
  public Optional<Chat> findChatById(long chatId) {
    return chatRepository.findById(chatId);
  }

  @Override
  public Chat addSeekerMessage(long chatId, long seekerId, String message) {
    Seeker seeker = seekerService.getSeekerObjectById(seekerId);
    Optional<Chat> chatOptional = findChatById(chatId);
    try {
      Chat chat = chatOptional.get();
      chat.addMessage(seeker, message);
      return chatRepository.save(chat);
    } catch (NoSuchElementException e) {
      return null;
    }
  }

  @Override
  public Chat addSellerMessage(long chatId, long sellerId, String message) {
    Seller seller = seekerService.getSellerObjectById(sellerId);
    Optional<Chat> chatOptional = findChatById(chatId);
    try {
      Chat chat = chatOptional.get();
      chat.addMessage(seller, message);
      return chatRepository.save(chat);
    } catch (NoSuchElementException e) {
      return null;
    }
  }
}
