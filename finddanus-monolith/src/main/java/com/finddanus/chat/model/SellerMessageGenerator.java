package com.finddanus.chat.model;

import com.finddanus.seller.model.Seller;

public class SellerMessageGenerator extends MessageGenerator {

  private Seller seller;

  public SellerMessageGenerator(Seller s) {
    this.seller = s;
  }

  @Override
  public Message createMessage(String text) {
    Message message = new Message(this.seller, text);
    return message;
  }
}
