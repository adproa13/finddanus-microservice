package com.finddanus.chat.model;

public abstract class MessageGenerator {
  public abstract Message createMessage(String text);
}