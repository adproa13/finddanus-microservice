package com.finddanus.chat.controller;

import com.finddanus.chat.service.ChatServiceImpl;
import com.finddanus.seeker.service.SeekerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ChatController {

  @Autowired
  private final ChatServiceImpl chatService;

  @Autowired
  private final SeekerService seekerService;

  /**
   * Controller of chat.
   * @param chatService of chat
   * @param seekerService of seeker
   */
  public ChatController(ChatServiceImpl chatService, SeekerService seekerService) {
    this.chatService = chatService;
    this.seekerService = seekerService;
  }

  @GetMapping("/chat/{chatId}")
  public String getSeekerChat(
      @PathVariable long chatId,
      Model model
  ) {
    model.addAttribute("chat", chatService.findChatById(chatId).get());
    return "chatSeeker";
  }

  @GetMapping("/seller/chat/{chatId}")
  public String getSellerChat(
      @PathVariable long chatId,
      Model model
  ) {
    model.addAttribute("chat", chatService.findChatById(chatId).get());
    return "chatSeller";
  }

  /**
   * Post mapping when seeker sends message.
   * @param redirectAttributes of attributes
   * @param seekerId of seeker
   * @param chatId of chat
   * @param message of seeker
   * @return redirects to chat view
   */
  @PostMapping("/chat/send")
  public String seekerSendMessage(
      RedirectAttributes redirectAttributes,
      @RequestParam(value = "seeker_id") long seekerId,
      @RequestParam(value = "chat_id") long chatId,
      @RequestParam(value = "message") String message
  ) {
    if (seekerId != -1) {
      redirectAttributes.addAttribute(
          "chat",
          chatService.addSeekerMessage(chatId, seekerId, message)
      );
      redirectAttributes.addAttribute(
          "chatId",
          chatService.findChatById(chatId).get()
      );
      return "redirect:/chat/{chatId}";
    }
    return "redirect:/chats";
  }

  /**
   * Post mapping when seller sends message.
   * @param redirectAttributes of attributes
   * @param sellerId of seller
   * @param chatId of chat
   * @param message of seller
   * @return redirect to chat view again
   */
  @PostMapping("seller/chat/send")
  public String sellerSendMessage(
      RedirectAttributes redirectAttributes,
      @RequestParam(value = "seller_id") long sellerId,
      @RequestParam(value = "chat_id") long chatId,
      @RequestParam(value = "message") String message
  ) {
    if (sellerId != -1) {
      redirectAttributes.addAttribute(
          "chat",
          chatService.addSellerMessage(chatId, sellerId, message)
      );
      redirectAttributes.addAttribute(
          "chatId",
          chatService.findChatById(chatId).get()
      );
      return "redirect:/seller/chat/{chatId}";
    }
    return "redirect:/seller/chats";
  }
}
