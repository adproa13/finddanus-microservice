package com.finddanus.login.service;

import com.finddanus.login.model.FindDanusUserDetails;
import com.finddanus.seeker.model.Seeker;
import com.finddanus.seeker.repository.SeekerRepository;
import com.finddanus.seller.model.Seller;
import com.finddanus.seller.repository.SellerRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class FindDanusUserDetailsService implements UserDetailsService {

  @Autowired
  private SeekerRepository seekerRepository;

  @Autowired
  private SellerRepository sellerRepository;

  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    Optional<Seeker> seekerOptional = seekerRepository.findByEmail(email);
    Optional<Seller> sellerOptional = sellerRepository.findByEmail(email);
    if (seekerOptional.isPresent()) {
      return new FindDanusUserDetails(seekerOptional.get());
    } else if (sellerOptional.isPresent()) {
      return new FindDanusUserDetails(sellerOptional.get());
    } else {
      throw new UsernameNotFoundException("User with email " + email + " not found.");
    }
  }
}
