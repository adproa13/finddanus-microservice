package com.finddanus.seeker.model;

import com.finddanus.seller.model.Subject;

public interface Observer {
  public void update();

  public void addFollowing(Subject subject);

  public void removeFollowing(Subject subject);

  public long getId();
}
