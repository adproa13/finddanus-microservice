package com.finddanus.seeker.model;

import com.finddanus.chat.model.Chat;
import com.finddanus.domain.model.User;
import com.finddanus.seller.model.Product;
import com.finddanus.seller.model.Seller;
import com.finddanus.seller.model.Subject;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "seeker")
public class Seeker extends User implements Observer {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "seeker_id", nullable = false, updatable = false)
  private long id;

  @Column(name = "role", columnDefinition = "varchar(255) default 'ROLE_SEEKER'")
  private String role;

  @OneToMany(
      mappedBy = "seeker",
      cascade = CascadeType.ALL,
      fetch = FetchType.LAZY
  )
  private List<Chat> chats;

  @ManyToMany(
      mappedBy = "followers",
      fetch = FetchType.EAGER,
      cascade = CascadeType.ALL,
      targetEntity = Seller.class
  )
  private List<Subject> following;

  @ManyToMany(
      fetch = FetchType.LAZY,
      cascade = CascadeType.ALL,
      targetEntity = Product.class
  )
  @JoinTable(
      name = "Seeker_Homepage",
      joinColumns = @JoinColumn(name = "seeker_id"),
      inverseJoinColumns = @JoinColumn(name = "p_id")
  )
  private List<Product> homepage;

  /**
   * Create user that looking for danus products.
   *
   * @param builder accepts builder object from SeekerBuilder
   */
  public Seeker(SeekerBuilder builder) {
    this.id = builder.getId();
    this.name = builder.getName();
    this.email = builder.getEmail();
    this.password = builder.getPassword();
    this.location = builder.getLocation();
    this.phoneNum = builder.getPhoneNum();
    this.chats = builder.getChats();
    this.following = builder.getFollowing();
    this.homepage = new ArrayList<>();
    role = "ROLE_SEEKER";
  }

  public Seeker() {}

  @Override
  public void update() {
    List<Product> updatedHomepage = new ArrayList<>();
    for (Subject seller : following) {
      updatedHomepage.addAll(seller.getProducts());
    }
    homepage = updatedHomepage;
  }

  @Override
  public void addFollowing(Subject seller) {
    following.add(seller);
    seller.addFollower(this);
    Chat chat = new Chat(this, (Seller) seller);
    addChat(chat);
    ((Seller) seller).addChat(chat);
  }

  @Override
  public void removeFollowing(Subject seller) {
    following.remove(seller);
    seller.removeFollower(this);
  }

  public List<Subject> getFollowing() {
    return this.following;
  }

  public List<Product> getProducts() {
    return this.homepage;
  }

  public void addChat(Chat chat) {
    this.chats.add(chat);
  }

  public void removeChat(Chat chat) {
    this.chats.remove(chat);
  }

  public List<Chat> getChats() {
    return chats;
  }

  public long getId() {
    return id;
  }

  public String getRole() {
    return role;
  }
}
